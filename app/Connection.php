<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Connection extends Model
{
	protected $fillable = [
        'user_id', 'connected_to', 'type',
    ];

    public function user(){
    	return $this->belongsTo(User::class, 'user_id');
    }

    public function connected(){
	    return $this->belongsTo(User::class, 'connected_to');
	}
	public function associated(){
	    return $this->belongsTo(User::class, 'user_id');
	}

    public function profile(){
    	return $this->belongsTo('\App\Profile', 'user_id');
    }


}