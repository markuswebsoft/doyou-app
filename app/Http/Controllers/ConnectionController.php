<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Connection as Connections;
use App\Profile as Profiles;
use Auth;
use DB;
use Session;
use Redirect;


class ConnectionController extends Controller
{
    public function connections($user){
         $connections = Auth::user()->connections;
        $connections->load('connected.profile');
        $associates = Auth::user()->associates;
        $associates->load('associated.profile');
        $associates = rekeyArray($associates->toArray(), "associated", "connected");

        $network = array_merge($connections->toArray(), $associates);

    	$data = [
			"page" => "connect",
    		"activeConnections" => $network, // active
    		"pendingConnections" => Connections::where('connected_to', Auth::user()->id)->where('type', 0)->get() // incoming requests
    	];

    	return view('layouts.connect.list', $data);
    }

    public function acceptConnect($user){
    	DB::table('connections')->where('connected_to', Auth::user()->id)->where('type', 0)->update([
    		'type' => 1
    	]);

    	Session::flash("message", [
            "alert" => "success",
            "header" => "Nice!",
            "body" => "You are now connected with " . Profiles::where('user_id', $user)->first()->handle
        ]);

    	return Redirect::back();

    }
}
