<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post as Posts;
use App\Profile as Profiles;
use App\Blog as Blogs;
use App\Notification as Notifications;
use Auth;
use Redirect;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // return view('home');
    }

    public function init(){
        // we change the app boot process by only showing the landing page if the user is not logged in. If they are logged in, they can start using the app rightaway. Think Facebook
        if(Auth::guest()){
            return view('welcome');
        }
        else{
            $posts = Posts::where('user_id', Auth::user()->id)->where('status', 1)->orderBy('created_at','desc')->get();

            foreach(Auth::user()->followings as $following){
                $posts = $posts->merge($following->posts);
            }

            if(Auth::user()->profileFinished()){
                $data = [
                    "page" => "home",
                    "posts" => $posts->sortByDesc('created_at'),
                    "mentions" => Notifications::where('user_id', Auth::user()->id)->get()
                ];
                
                return view('home', $data);
            }

            return view('layouts.profile.finish');
        }
    }

    public function search(Request $request){

        if(Auth::guest()){
            return view('welcome');
        }

        $data = [
            "page" => "home",
            "profiles" => Profiles::search()->orderBy('handle')->get(),
            "posts" => Posts::search()->orderBy('title')->get(),
            "blogs" => Blogs::search()->orderBy('title')->get(),
            "query" => $request->search
        ];

        //dd($data);

        return view('search', $data);
    }
}
