<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile as Profiles;
use App\Message as Messages;
use App\Follow as Follows;
use App\Connect as Connections;

use Auth;
use DB;

class MessageController extends Controller
{

	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function messages(){
    	$messages = Messages::select('*')
    					->orderBy('created_at', 'desc')
    					->where('user_id', Auth::user()->id)
    					->orWhere('receiver', Auth::user()->id)
    					->get()
                        ->unique('conversation_id');

        


    	$data = [
            "page" => "home",
    		"messages" => $messages,
            "convers"
    	];

        return view('layouts.message.index', $data);
    }

    public function conversation($user){
        $data = [
            "page" => "home",
        	"receiver" => Profiles::where('user_id', $user)->first()
        ];

        return view('layouts.message.conversation', $data);
    }

    public function message(Request $request){
        // 
        if(Messages::where('user_id', Auth::user()->id)->orWhere('receiver', Auth::user()->id)->count() == 0){
            Messages::create([
                "user_id" => Auth::user()->id,
                "conversation_id" => generateRandomNumString(),
                "receiver" => $request->receiver,
                "title" => null,
                "message" => $request->message
            ]);
        }
        else{
            $conversationID = Messages::where('user_id', Auth::user()->id)->orWhere('receiver', Auth::user()->id)->orderBy('created_at')->first()->conversation_id;

            Messages::create([
                "user_id" => Auth::user()->id,
                "conversation_id" => $conversationID,
                "receiver" => $request->receiver,
                "title" => null,
                "message" => $request->message
            ]);
        }
    	
    }

    public function fetchConversation(){
    	$html = "";

    	foreach(Messages::where('user_id', Auth::user()->id)->orWhere('receiver', Auth::user()->id)->orderBy('created_at', 'asc')->get() as $message)
    	{
    		$reversed = "";
    		if($message->user_id == Auth::user()->id){
    			$reversed = "reversed";
    		}

    		$html .= '<li class="media ' . $reversed . '">
						<div class="media-left">
							<a href="assets/images/demo/images/3.png">
								<img src="assets/images/demo/users/face11.jpg" class="img-circle img-md" alt="">
							</a>
						</div>

						<div class="media-body">
							<div class="media-content">' . $message->message .  ' </div>
                            <span class="media-annotation display-block mt-10">' . convertTimeStamp($message->created_at) . '</span>
						</div>
					</li>';
		}
		return $html;
    }

    public function compose(){
        $data = [
            "page" => "home",
            "followings" => Auth::user()->following(),
            "followers" => Auth::user()->followers(),
            "connections" => Auth::user()->connections()
        ];

        return view('layouts.message.compose', $data);
    }
}
