<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Post as Posts;
use App\User as Users;
use App\Comment as Comments;
use App\Like as Likes;
use App\Notification as Notifications;
use Auth;
use DB;
use Session;
use Redirect;
use Storage;

class NotificationsController extends Controller
{
    public function unread($user){
    	$notifications = [];
    	foreach(Users::find($user)->notifications->where('is_read', 0)->where('from_id', '!=', Auth::user()->id) as $notification){

    		$notify = [
    			"handle" => "@" . Users::find($notification->from_id)->profile->handle,
    			"avatar" => Users::find($notification->from_id)->avatar()
    		];

            $notify["text"] = mentionText($notification->from_id, $notification->type, $notification->post_id, true, $notification->comment_id);

    		array_push($notifications, $notify);
    	}

    	Notifications::where('user_id', $user)->update([
    		"is_read" => 1
		]);

    	return json_encode($notifications);
    }
}
