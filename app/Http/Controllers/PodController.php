<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog as Blogs;
use App\Subject as Subjects;
use App\Shade as Shades;
use Auth;
use Redirect;
use Session;

class PodController extends Controller
{
    public function view($url){
		$blog =  Blogs::where('url', $url)->first();

		$data = [
			"blog" => $blog
		];

		if($blog->privacy == 1){
			return view("layouts.podium.public-view", $data);
		}
		elseif ($blog->privacy == 2) {
			if(Auth::guest()){
				return view("layouts.podium.not-public", $data);
			}

			if($blog->user->imFollowingThem($blog->user_id) || $blog->user->imConnectedToThem($blog->user_id)){
				return view("layouts.podium.view", $data);
			}
		}
    }
}
