<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post as Posts;
use App\User as Users;
use App\Comment as Comments;
use App\Like as Likes;
use App\Profile as Profiles;
use DB;
use Auth;
use Session;
use Redirect;
use Storage;
use Notify;

class PostsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view($post){
        $data = [
            "page" => "home",
            "post" => Posts::find($post)
        ];

        return view("layouts.posts.view", $data);
        
    }
    
    public function publish(Request $request){

    	$mediaDestination = public_path() . "/uploads/media";


    	if($request->file('media') !== null){
            $media = $request->file('media');
            $mediaName = generateRandomString() . "-" . $media->getClientOriginalName();
            $media->move($mediaDestination, $mediaName); 

            DB::table('media')->insert([
                "user_id" => Auth::user()->id,
                "path" => $mediaName
            ]);

            $media_id = DB::getPdo()->lastInsertId();

            DB::table('posts')->insert([
    		"user_id" => Auth::user()->id,
    		"type" => 2, // media post
    		"content" => $request->content,
    		"featured" => $media_id
			]);

        } else {

            DB::table('posts')->insert([
        		"user_id" => Auth::user()->id,
        		"content" => $request->content
    		]);
        }

        $postID = DB::getPdo()->lastInsertId();

		Session::flash("message", [
            "alert" => "success",
            "header" => "Nice!",
            "body" => "Your post was submitted successfully"
        ]);

        $string = $request->content;
        preg_match_all('/@([A-Za-z0-9_]{1,15})/', $string, $usernames);

        foreach($usernames[1] as $handle){
            $profile = Profiles::where('handle', $handle)->first();
            Notify::mentionedYouPublish(Auth::user()->id, $profile->user_id, $postID);
        }

        return Redirect::back();
    }

    public function comment(Request $request){
        $post = Posts::find($request->post);
        // posting comment to someone's post
        Comments::create([
            "user_id" => Auth::user()->id,
            "post_id" => $post->id,
            "comment" => $request->comment
        ]);

        // notify the user



        preg_match_all('/@([A-Za-z0-9_]{1,15})/', $request->comment, $usernames);

        foreach($usernames[1] as $handle){
            $profile = Profiles::where('handle', $handle)->first();
            Notify::postCommentNewThread(Auth::user()->id, $profile->user_id, $post->id);
        }

        
        
    }

    public function commentReply(Request $request){
        Comments::create([
            "user_id" => Auth::user()->id,
            "post_id" => $request->post,
            "comment_id" => $request->reply,
            "comment" => $request->comment
        ]);
    }

    public function like(Request $request){
        Likes::create([
            "user_id" => Auth::user()->id,
            "post_id" => $request->post
        ]);

        Notify::likedPost(Auth::user()->id, Posts::find($request->post)->user_id, $request->post);
    }
    public function unlike(Request $request){
        Likes::where("user_id",Auth::user()->id)
            ->where("post_id",$request->post)
            ->delete();
    }

    public function deletePost(Request $request){
        $post = Posts::find($request->post);
        
        if($post->isOwner()){
            $post->delete();
        }
    }

    public function edit($post){
        $data = [
            "post" => Posts::find($post),
            "page" => "home"
        ];

        return view('layouts.posts.edit', $data);
    }

    public function update(Request $request){
        $post = Posts::find($request->post_id);

        if($post->isOwner()){

            $mediaDestination = public_path() . "/uploads/media";

            if($request->file('media') !== null){
                $media = $request->file('media');
                $mediaName = generateRandomString() . "-" . $media->getClientOriginalName();
                $media->move($mediaDestination, $mediaName); 

                DB::table('media')->insert([
                    "user_id" => Auth::user()->id,
                    "path" => $mediaName
                ]);

                $media_id = DB::getPdo()->lastInsertId();

                 DB::table('posts')->where('id', $post->id)->update([
                    "featured" => $media_id
                ]);

            }

            if($request->remove_featured == 1){
                DB::table('posts')->where('id', $post->id)->update([
                    "featured" => NULL
                ]);
            }

            DB::table('posts')->where('id', $post->id)->update([
                "content" => $request->content
            ]);


            Session::flash("message", [
                "alert" => "success",
                "header" => "Nice!",
                "body" => "Your post was updated"
            ]);
        }

        return Redirect::back();
    }
}
