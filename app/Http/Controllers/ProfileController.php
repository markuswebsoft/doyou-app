<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Profile as Profiles;
use App\Avatar as Avatars;
use App\Banner as Banners;
use Session;
use Redirect;
use Auth;

class ProfileController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

	public function finishProfile(Request $request){

		DB::table('profiles')->insert([
			'user_id' => Auth::user()->id,
			'handle' => $request->handle,
			'bio' => $request->bio,
			'location' => $request->location,
			'website' => $request->website
		]);

		// upload the avatar

		$avatarDestination = public_path() . "/uploads/avatars";

		if($request->file('avatar') !== null){
            $avatar = $request->file('avatar');
            $avatarName = generateRandomString() . "-" . $avatar->getClientOriginalName();
            $avatar->move($avatarDestination, $avatarName); 

            DB::table('avatars')->insert([
                "user_id" => Auth::user()->id,
                "path" => $avatarName,
            ]);

            $avatar_id = DB::getPdo()->lastInsertId();
            DB::table('profiles')->where('user_id', Auth::user()->id)->update([
            	'avatar_id' => $avatar_id
        	]);
        }

        // upload profile banner

		$bannerDestination = public_path() . "/uploads/banners";

		if($request->file('banner') !== null){
            $banner = $request->file('banner');
            $bannerName = generateRandomString() . "-" . $banner->getClientOriginalName();
            $banner->move($bannerDestination, $bannerName); 

            DB::table('banners')->insert([
                "user_id" => Auth::user()->id,
                "path" => $bannerName,
            ]);

            $banner_id = DB::getPdo()->lastInsertId();
            DB::table('profiles')->where('user_id', Auth::user()->id)->update([
            	'banner_id' => $banner_id
        	]);
        }

        Session::flash("message", [
            "alert" => "success",
            "header" => "Nice!",
            "body" => "Your profile setup is complete"
        ]);

        return Redirect::to('/');
	}

	public function confirmUsername(Request $request){
		if(strlen($request->username) >= 4 && strlen($request->username) <= 12){
			if (Profiles::where('handle', $request->username)->count() == 0){
				return "ok";
			}
		}
	}

    public function editProfileView($user){
        $profile = Profiles::where('user_id', $user)->first();

        $data = [
            "page" => "home",
            "profile" => $profile,
            "avatar" => Avatars::where('id', $profile->avatar_id)->first(),
            "banner" => Banners::where('id', $profile->banner_id)->first()
        ];

        return view('layouts.profile.edit', $data);

    }

    public function editProfile(Request $request){
        DB::table('profiles')->where('user_id', Auth::user()->id)->update([
            'handle' => $request->handle,
            'bio' => $request->bio,
            'location' => $request->location,
            'website' => $request->website,
            'color' => $request->color,
            'dob' => $request->dob
        ]);

        // upload the avatar

        $avatarDestination = public_path() . "/uploads/avatars";

        if($request->file('avatar') !== null){
            $avatar = $request->file('avatar');
            $avatarName = generateRandomString() . "-" . $avatar->getClientOriginalName();
            $avatar->move($avatarDestination, $avatarName); 

            Avatars::updateOrCreate([
                "user_id" => Auth::user()->id,
                "path" => $avatarName
                ]);
            /*
            DB::table('avatars')->insert([
                "user_id" => Auth::user()->id,
                "path" => $avatarName,
            ]);
            */

            $avatar_id = DB::getPdo()->lastInsertId();
            DB::table('profiles')->where('user_id', Auth::user()->id)->update([
                'avatar_id' => $avatar_id
            ]);
        }

        // upload profile banner

        $bannerDestination = public_path() . "/uploads/banners";

        if($request->file('banner') !== null){
            $banner = $request->file('banner');
            $bannerName = generateRandomString() . "-" . $banner->getClientOriginalName();
            $banner->move($bannerDestination, $bannerName); 

            Banners::updateOrCreate([
                "user_id" => Auth::user()->id,
                "path" => $bannerName
                ]);
            /*
            DB::table('banners')->insert([
                "user_id" => Auth::user()->id,
                "path" => $bannerName,
            ]);
            */

            $banner_id = DB::getPdo()->lastInsertId();
            DB::table('profiles')->where('user_id', Auth::user()->id)->update([
                'banner_id' => $banner_id
            ]);
        }

        Session::flash("message", [
            "alert" => "success",
            "header" => "Nice!",
            "body" => "Your profile has been updated"
        ]);

        return Redirect::back();
    }
}
