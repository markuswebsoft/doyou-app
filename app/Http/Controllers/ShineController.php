<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog as Blogs;
use App\Subject as Subjects;
use App\Shade as Shades;
use Auth;
use DB;
use Session;
use Redirect;

class ShineController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(){

        $data = [
            "page" => "shine"
        ];

        return view('layouts.shine.index', $data);
    }
}
