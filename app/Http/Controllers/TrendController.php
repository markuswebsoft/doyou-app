<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Blog as Blogs;
use App\Post as Posts;
use App\Profile as Profiles;
use Session;
use Redirect;
use Auth;

class TrendController extends Controller
{
    public function hashtag($tag){

    	$data = [
    		"page" => "home",
    		"posts" => Posts::where('content', 'LIKE', '%'.$tag.'%')->get()->sortByDesc('created_at')
    	];

    	return view('trend', $data);
    }
}
