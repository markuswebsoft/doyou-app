<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User as Users;
use App\Profile as Profiles;
use App\Post as Posts;
use App\Follow as Follows;
use App\Connection as Connections;
use App\Notification as Notifications;
use Auth;
use Redirect;
use Session;
use Notify;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile($id){
        $connections = Users::find($id)->connections;
        $connections->load('connected.profile');
        $associates = Users::find($id)->associates;
        $associates->load('associated.profile');
        $associates = rekeyArray($associates->toArray(), "associated", "connected");

        $network = array_merge($connections->toArray(), $associates);

    	$owner = false;
    	if($id == Auth::user()->id){
    		$owner = true;
    	}
    	$data = [
            "page" => "home",
    		"profile" => Profiles::where('user_id', $id)->first(),
            "posts" => Posts::where('user_id', $id)->orderBy('created_at','desc')->get(),
    		"owner" => $owner,
            "network" => $network
    	];

    	return view('layouts.profile.user', $data);
        
    }

    public function follow($user){
        Follows::create([
            "user_id" => Auth::user()->id,
            "following" => $user
        ]);

        Notify::followedYou(Auth::user()->id, $user);

        return "ok";
    }

    public function unfollow($user){
        Follows::where("user_id", Auth::user()->id)->where("following", $user)->delete();
        return "ok";
    }

    public function connect($user){
        Connections::create([
            "user_id" => Auth::user()->id,
            "connected_to" => $user
        ]);

        Notify::wantsToConnect(Auth::user()->id, $user);

        
        return "ok";
    }

    public function disconnect($user){
        Connections::where("user_id", Auth::user()->id)->where("connected_to", $user)->delete();
        return "ok";
    }

    public function handles(Request $request){
        $handle = $request->handle;
        $response = [];


        foreach(Profiles::where('handle', 'like', "%{$handle}%")->get() as $profile){
            $profile = [
                "id" => $profile->user_id, 
                "name" => "@" . $profile->handle,
                "avatar" => $profile->user->avatar(),
                "type" => "contact"
            ];

            $response[0] = $profile;
        }

        return json_encode($response, 128);
    }

    public function mentions($user){

        $data = [ 
            "page" => "home",
            "mentions" => Notifications::where('user_id', Auth::user()->id)->get()
        ];

        return view('mentions', $data);
    }
}
