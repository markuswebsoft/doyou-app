<?php

use Carbon\Carbon;

use App\User as User;
use App\Profile as Profiles;
use App\Avatar as Avatars;
use App\Message as Messages;
use App\Media as Media;
use App\Post as Posts;
use App\Comment as Comments;

function getAvatar($id)
{
    return "/uploads/avatars/" . Avatars::where('id', Profiles::where('user_id', $id)->first()->avatar_id)->first()->path;
}



function convertHTMLTime($time)
{
    return date("m-d-Y", strtotime($time));
}

function convertTimestamp($stamp, $pull = null)
{
    if($pull == "month"){
        return date('m', strtotime($stamp . " America/New_York"));
    }
    return date('M. j, Y', strtotime($stamp . " America/New_York"));
}

function DateDifference($start, $end){
    $datetime1 = new DateTime($start);
    $datetime2 = new DateTime($end);
    $interval = $datetime1->diff($datetime2);
    return $interval->format('%R%a');
}

function convertHTMLTime2($date){
    $parts = explode('-',$date);
    $yyyy_mm_dd = $parts[1] . '-' . $parts[2] . '-' . $parts[0];
    return $yyyy_mm_dd;
}

function month($m){
$monthNum  = $m;
$dateObj   = DateTime::createFromFormat('!m', $monthNum);
return $dateObj->format('F'); // March
}

function exposeArray($array){
    echo "<pre>";
    print_r($array);
    echo "</pre>";
}

function rekeyArray($array, $oldkey, $newkey){
    for($i = 0; $i < sizeof($array); $i++){
        $array[$i][$newkey] = $array[$i][$oldkey];
        unset($array[$i][$oldkey]);
    }
    return $array;
}

function validateEmail($email){
    if(User::where('id','!=', Auth::user()->id)->where('email', $email)->count() == 0){
        return true;
    }
    return false;
}

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function generateRandomNumString($length = 10) {
    $characters = '0123456789';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function linkHashes($string){
    preg_match("/#(\\w+)/", $string, $matches);
    foreach($matches as $match){
        $string = str_replace("#".$match, '<a href="/trend/' . str_replace("#", "", $match)  . '">#'. $match . '</a>', $string);
    }

    return $string;
}

function mentionText($user, $type, $post, $showHandle, $comment){
    $handle = Profiles::where('user_id', $user)->first()->handle;
    $notification = "";
    if($showHandle){
        $notification = "@" . $handle. " ";  
    }
    switch ($type) {
        case 0:
            return " commented on your post";
            break;
        case 1:
            $comment = Comments::find($comment);
            $notification .= "mentioned you in a comment: <br> $comment";
            return $notification;
            break;
        case 2:
            $message = Messages::find($post)->message;
            $notification .= "messaged you: <br> $message";
            return $notification;
            break;
        case 3:
            return " liked your post";
            break;
        case 4:
            return " followed you";
            break;
        case 5:
            return " sent a connection request";
            break;
        case 6:
            return " mentioned you in their post";
            break;
        case 7:
            return " mentioned you in a discussion";
            break;
        default:
            return "";
            break;
    }
}

