<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User as Users;
use Auth;

class Message extends Model
{
	protected $fillable = [
        'user_id', 'receiver', 'title', 'message', 'type', 'conversation_id'
    ];

    public function user(){
    	return $this->belongsTo('\App\User');
    }

    public function profile(){
    	return $this->belongsTo('\App\Profile');
    }

    public function scopeConversationWithId($query){
    	if($this->user_id == Auth::user()->id){
    		$receiver = $query->where('conversation_id', $this->conversation_id)->first()->receiver;
    		return Users::where('id', $receiver)->first()->id;
    	} else {
    		$receiver = $query->where('conversation_id', $this->conversation_id)->first()->user_id;
    		return Users::where('id', $receiver)->first()->id;
    	}
    }

    public function scopeConversationWithName($query){
    	if($this->user_id == Auth::user()->id){
    		$receiver = $query->where('conversation_id', $this->conversation_id)->first()->receiver;
    		return "@" . Users::where('id', $receiver)->first()->profile->handle;
    	} else {
    		$receiver = $query->where('conversation_id', $this->conversation_id)->first()->user_id;
    		return "@" . Users::where('id', $receiver)->first()->profile->handle;
    	}
    }

    public function scopeConversationWithAvatar($query){
    	if($this->user_id == Auth::user()->id){
    		$receiver = $query->where('conversation_id', $this->conversation_id)->first()->receiver;
    		return Users::where('id', $receiver)->first()->avatar();
    	} else {
    		$receiver = $query->where('conversation_id', $this->conversation_id)->first()->user_id;
    		return Users::where('id', $receiver)->first()->avatar();
    	}
    }
}