<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
	protected $fillable = [
        'user_id', 'from_id', 'type', 'is_read', 'message', 'post_id', 'comment_id',
    ];

    public function user(){
    	return $this->belongsTo('\App\User');
    }

    public function author(){
        return $this->belongsTo('\App\User', 'from_id');
    }

    public function scopeUnread($query){
    	return $query->where('is_read', 0)->get();
    }

    public function markRead($query){
    	return $query->where('id', $this->id)->update(['is_read', 1]);
    }

    public function profile(){
        return $this->author->profile();
        //Users::find($notification->from_id)->profile->handle
    }
}
