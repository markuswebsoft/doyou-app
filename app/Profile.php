<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
	protected $table = 'profiles';
	
	public function user(){
    	return $this->belongsTo('\App\User');
    }

    public function messages(){
    	return $this->hasMany('\App\Message');
    }

    public function scopeSearch($q){
    	 return empty(request()->search) ? $q : $q->where('handle', 'like', '%'.request()->search.'%')->orWhere('bio', 'like', '%'.request()->search.'%');

    }
}