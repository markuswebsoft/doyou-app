<?php

namespace App\Services\Email;
use Auth;
use DB;
use Mail;
use App\Message as Messages;
use App\User as Users;
use App\Profile as Profiles;
use App\Experience as Experiences;
use App\Payment as Payments;

class Email
{
	public function send($user, $subject, $header, $content){
		$user = Users::where('id', $user)->first();
		$profile = Profiles::where('user_id', $user->id)->first();
		$name = "$profile->first_name $profile->last_name";
		$email = $user->email;

		$data = [
        	"name" => $name,
        	"header" => $header,
        	"body" => $content
    	];

    	Mail::send(['html' => 'emails.generic'], $data, function($message) use ($subject, $email, $name){
        $message->to($email, $name)
        ->subject("$subject");
        	$message->from("certificationteam@artba.org",'ARTBA Certification Team');
    	});
	}

	public function receipt($user, $invoice){
		$user = Users::where('id', $user)->first();
		$profile = Profiles::where('user_id', $user->id)->first();
		$name = "$profile->first_name $profile->last_name";
		$email = $user->email;
        $subject = "Your SCTPP Application Receipt";

		$data = [
        	"name" => $name,
        	"invoice" => $invoice
    	];

    	Mail::send(['html' => 'emails.receipt'], $data, function($message) use ($subject, $email, $name){
        $message->to($email, $name)
        ->subject("$subject");
        	$message->from("certificationteam@artba.org",'ARTBA Certification Team');
    	});
	}

    public function retestReceipt($user, $invoice){
        $user = Users::where('id', $user)->first();
        $profile = Profiles::where('user_id', $user->id)->first();
        $name = "$profile->first_name $profile->last_name";
        $email = $user->email;
        $subject = "Your SCTPP Testing Receipt";

        $data = [
            "name" => $name,
            "invoice" => $invoice
        ];

        Mail::send(['html' => 'emails.retest-receipt'], $data, function($message) use ($subject, $email, $name){
        $message->to($email, $name)
        ->subject("$subject");
            $message->from("certificationteam@artba.org",'ARTBA Certification Team');
        });
    }

	public function verification($user, $experience){
		$user = Users::where('id', $user)->first();
		$profile = Profiles::where('user_id', $user->id)->first();
		$experience = Experiences::where('secret', $experience)->first();

		$name = "$experience->reference";
		$subject = "Please Verify Work Experience for $profile->first_name $profile->last_name";
		$email = $experience->email;

		$data = [
        	"user" => $user,
        	"experience" => $experience,
        	"profile" => $profile
    	];

    	Mail::send(['html' => 'emails.verification'], $data, function($message) use ($subject, $email, $name){
        $message->to($email, $name)
        ->subject("$subject");
        	$message->from("certificationteam@artba.org",'ARTBA Certification Team');
    	});
	}
}