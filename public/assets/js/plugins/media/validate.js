jQuery(document).ready(function ($) {
window.URL    = window.URL || window.webkitURL;
    var banner  = document.getElementById("banner"),
    bannerPreview = document.getElementById("bannerPreview"),
    useBlob   = false && window.URL; // Set to `true` to use Blob instead of Data-URL

    var avatar  = document.getElementById("avatar"),
    avatarPreview = document.getElementById("avatarPreview"),
    useBlob   = false && window.URL; // Set to `true` to use Blob instead of Data-URL

// 2.
function readImage (file, trigger) {

  // Create a new FileReader instance
  // https://developer.mozilla.org/en/docs/Web/API/FileReader


  var reader = new FileReader();

  // Once a file is successfully readed:
  reader.addEventListener("load", function () {

    // At this point `reader.result` contains already the Base64 Data-URL
    // and we've could immediately show an image using
    // `bannerPreview.insertAdjacentHTML("beforeend", "<img src='"+ reader.result +"'>");`
    // But we want to get that image's width and height px values!
    // Since the File Object does not hold the size of an image
    // we need to create a new image and assign it's src, so when
    // the image is loaded we can calculate it's width and height:
    var image  = new Image();
    image.addEventListener("load", function () {
        
        

      // Concatenate our HTML image info 
      var imageInfo = '';

      // Finally append our created image and the HTML info string to our `#preview` 
      if(trigger == "banner"){
        bannerPreview.appendChild( this );
      bannerPreview.insertAdjacentHTML("beforeend", imageInfo +'<br>');
      } else {
        avatarPreview.appendChild( this );
      avatarPreview.insertAdjacentHTML("beforeend", imageInfo +'<br>');
      }
      
      
      
      
      
      // If we set the variable `useBlob` to true:
      // (Data-URLs can end up being really large
      // `src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAADAAAAA...........etc`
      // Blobs are usually faster and the image src will hold a shorter blob name
      // src="blob:http%3A//example.com/2a303acf-c34c-4d0a-85d4-2136eef7d723"
      if (useBlob) {
        // Free some memory for optimal performance
        window.URL.revokeObjectURL(image.src);
      }
    });
        
    image.src = useBlob ? window.URL.createObjectURL(file) : reader.result;

  });

  // https://developer.mozilla.org/en-US/docs/Web/API/FileReader/readAsDataURL
  reader.readAsDataURL(file);  
}

function verifyExtensions(extension){
    if(extension == "image/jpg" || extension == "image/jpeg" || extension == "image/png" || extension == "image/gif"){
        userNotify("2", 1);
        return true;
    }
    userNotify("2", 2);
    return false;
}

function verifySize(size){
    if(size <= 80){
        userNotify("1", 1);
        return true;
    }
    userNotify("1", 2);
    return false;
}

function verifyDimensions(x,y){
    var n = x + "" + y;

    switch(n) {
    case "160600":
        userNotify("3", 1);
        return true;
        break;
    case "72890":
        userNotify("3", 1);
        return true;
        break;
    case "120600":
        userNotify("3", 1);
        return true;
    break;
    case "300250":
        userNotify("3", 1);
        return true;
    break;
    case "336280":
        userNotify("3", 1);
        return true;
    break;
    case "30050":
        userNotify("3", 1);
        return true;
    break;
    case "46860":
        userNotify("3", 1);
        return true;
    break;
    case "970250":
        userNotify("3", 1);
        return true;
    break;
    case "970250":
        userNotify("3", 1);
        return true;
    break;
    case "200200":
        userNotify("3", 1);
        return true;
    break;
    case "32050":
        userNotify("3", 1);
        return true;
    break;
    default:
        userNotify("3", 2);
        return false;
    }
}

function userNotify(id, status){
    if(status == "1"){
        $("#q" + id).removeClass("fa-times");
        $("#q" + id).addClass("fa-check");
        return true;
    } else {
        $("#q" + id).removeClass("fa-check");
        $("#q" + id).addClass("fa-times");
    }
    return false;
}

// 1.
// Once the user selects all the files to upload
// that will trigger a `change` event on the `#browse` input
banner.addEventListener("change", function() {
  $("#bannerPreview").empty();
  // Let's store the FileList Array into a variable:
  // https://developer.mozilla.org/en-US/docs/Web/API/FileList
  var files  = this.files;
  // Let's create an empty `errors` String to collect eventual errors into:
  var errors = "";

  if (!files) {
    errors += "File upload not supported by your browser.";
  }

  // Check for `files` (FileList) support and if contains at least one file:
  if (files && files[0]) {

    // Iterate over every File object in the FileList array
    for(var i=0; i<files.length; i++) {

      // Let's refer to the current File as a `file` variable
      // https://developer.mozilla.org/en-US/docs/Web/API/File
      var file = files[i];

      // Test the `file.name` for a valid image extension:
      // (pipe `|` delimit more image extensions)
      // The regex can also be expressed like: /\.(png|jpe?g|gif)$/i
      if ( (/\.(png|jpeg|jpg|gif)$/i).test(file.name) ) {
        // SUCCESS! It's an image!
        // Send our image `file` to our `readImage` function!
        readImage( file  , "banner"); 
        $("#reset").show();
      } else {
        errors += file.name +" Unsupported Image extension\n";  
      }
    }
  }

  // Notify the user for any errors (i.e: try uploading a .txt file)
  if (errors) {
    $("#banner").val("");
    $('#banner').parent().find(".filename").text("Select another file");
    alert(errors);
  }

});

$("#reset").click(function(){
  var text = $("#post-text").val();
  $("#new-post").find(".filename").text("Select another file");
  $('#new-post')[0].reset();
  $("#post-text").val(text);
  $("#bannerPreview").empty();
  $(this).hide();
});

avatar.addEventListener("change", function() {
$("#avatarPreview").empty();
  // Let's store the FileList Array into a variable:
  // https://developer.mozilla.org/en-US/docs/Web/API/FileList
  var files  = this.files;
  // Let's create an empty `errors` String to collect eventual errors into:
  var errors = "";

  if (!files) {
    errors += "File upload not supported by your browser.";
  }

  // Check for `files` (FileList) support and if contains at least one file:
  if (files && files[0]) {

    // Iterate over every File object in the FileList array
    for(var i=0; i<files.length; i++) {

      // Let's refer to the current File as a `file` variable
      // https://developer.mozilla.org/en-US/docs/Web/API/File
      var file = files[i];

      // Test the `file.name` for a valid image extension:
      // (pipe `|` delimit more image extensions)
      // The regex can also be expressed like: /\.(png|jpe?g|gif)$/i
      if ( (/\.(png|jpeg|jpg|gif)$/i).test(file.name) ) {
        // SUCCESS! It's an image!
        // Send our image `file` to our `readImage` function!
        readImage( file , "avatar"); 
      } else {
        errors += file.name +" Unsupported Image extension\n";  
      }
    }
  }

  // Notify the user for any errors (i.e: try uploading a .txt file)
  if (errors) {
    $("#avatar").val("");
    $('#avatar').parent().find(".filename").text("Select another file");
    alert(errors);
    $("#reset").hide();
  }

});
});