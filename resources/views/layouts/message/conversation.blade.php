@extends('layouts.application')

@section('content')
<div class="panel panel-flat">
						<div class="panel-heading">
							<a href="{{ route('user', ['id' => $receiver->user_id]) }}">
								<h6 class="panel-title"><ul class="breadcrumb">
							<li><a href="{{ route('user-messages') }}">Conversations</a></li>
							<li class="active"><a href="{{ route('user', ['user' => $receiver->user_id  ]) }}"><img src="{{ $receiver->user->avatar() }}" width="30px" height="30px" style="max-height: inherit !important;"> {{ "@" . $receiver->handle }}</a></li>
						</ul></h6>
							</a>
							
						</div>

						<div class="panel-body">
							<ul class="media-list chat-list content-group" id="chat-list"></ul>

	                    	<textarea id="message" name="enter-message" class="form-control content-group" rows="3" cols="1" placeholder="Enter your message..."></textarea>

	                    	<div class="row">
	                    		<div class="col-xs-12 text-right">
		                            <button id="send" data-receiver="{{ $receiver->user_id }}" type="button" class="btn bg-teal-400 btn-labeled btn-labeled-right send"><b><i class="icon-circle-right2"></i></b> Send</button>
	                    		</div>
	                    	</div>
						</div>
					</div>
@endsection

@section('js')
	<script type="text/javascript">
    jQuery(function($) {
    	fetchConversation();

		$("#send").click(function(){
			sendMessage();
		});
		$("#message").keypress(function(event) {
		    if(event.keyCode == 13) {
		        sendMessage();
		    }
		});

		function sendMessage(){
			var receiver = $("#send").attr("data-receiver");
			var message = $("#message").val();
			var today = moment().format("MMM. D, Y"); 
			if(message != ""){
				$.post( "{{ route('message') }}", { receiver: receiver, message: message , _token: "{{ CSRF_TOKEN() }}"}, function(data, status){
					fetchConversation();
					$("#message").val("");
				});
			}
		}

		function fetchConversation(){
		    var feedback = $.ajax({
		        type: "GET",
		        url: "/messages/conversation/fetch/{{ $receiver->user_id }}",
		        async: false,
		        success: function(feedback){
		        	if(feedback == $("#chat-list").html()){
		        		console.log("no updates");
		        	} else {
		        		$('#chat-list').html(feedback);
		        	}
		        	
			        setTimeout(function(){fetchConversation();}, 4000);
			    }
		});

    
}
	});
	</script>
@endsection