<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>doYou</title>

        <!-- Global stylesheets -->
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
        <link href="/assets/css/icons/icomoon/styles.css" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">

        <link href="/assets/css/bootstrap.css" rel="stylesheet" type="text/css">
        <link href="/assets/css/core.css" rel="stylesheet" type="text/css">
        <link href="/assets/css/components.css" rel="stylesheet" type="text/css">
        <link href="/assets/css/colors.css" rel="stylesheet" type="text/css">
        <!-- /global stylesheets -->

        <link href="/assets/css/style.css" rel="stylesheet" type="text/css">
        <link href="/assets/css/mention.css" rel="stylesheet" type="text/css">
        @yield('css')
        <style>
            .img-rounded.white-bg{
                background:white;
                border-radius: 50%;
                padding:5px;
                width:50px;
                margin: 0 auto;
            }
            .brighttheme-success{
                padding:0 !important;
            }
            .pl-10{
                padding-left:10px;
                vertical-align: middle;
                padding-top:0;
            }
        </style>
        <link href="/css/responsive.css" rel="stylesheet" type="text/css">
    </head>
    <body class="navbar-top">
        @include('layouts.navbar')
        <div class="page-container">
            <div class="page-content">
                @include('layouts.podium.sidebar')
                <div class="content-wrapper">
                    <div class="content">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>

        @include('layouts.modals')

    <!-- Core JS files -->
        <script type="text/javascript" src="/assets/js/plugins/loaders/pace.min.js"></script>
        <script type="text/javascript" src="/assets/js/core/libraries/jquery.min.js"></script>
        <script type="text/javascript" src="/assets/js/core/libraries/bootstrap.min.js"></script>
        <script type="text/javascript" src="/assets/js/plugins/loaders/blockui.min.js"></script>
        <!-- /core JS files -->

        <!-- Theme JS files -->
        <script type="text/javascript" src="/assets/js/plugins/forms/selects/select2.min.js"></script>
        <script type="text/javascript" src="/assets/js/plugins/forms/styling/uniform.min.js"></script>
        <script type="text/javascript" src="/assets/js/plugins/ui/moment/moment.min.js"></script>
        <!-- Theme JS files -->
        <script type="text/javascript" src="/assets/js/plugins/notifications/pnotify.min.js"></script>
        <script type="text/javascript" src="/assets/js/pages/components_notifications_pnotify.js"></script>
        <script type="text/javascript" src="/assets/js/core/app.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.min.js" integrity="sha256-CutOzxCRucUsn6C6TcEYsauvvYilEniTXldPa6/wu0k=" crossorigin="anonymous"></script>
        <!-- /theme JS files -->

        <!-- Theme JS files -->

        <!-- Theme JS files -->
        <script type="text/javascript" src="/assets/js/plugins/forms/wizards/steps.min.js"></script>
        <script type="text/javascript" src="/assets/js/core/libraries/jasny_bootstrap.min.js"></script>
        <script type="text/javascript" src="/assets/js/plugins/forms/validation/validate.min.js"></script>
        <script type="text/javascript" src="/assets/js/plugins/extensions/cookie.js"></script>

        <script type="text/javascript" src="/assets/js/pages/wizard_steps.js"></script>
        <script type="text/javascript" src="/assets/js/pages/form_inputs.js"></script>
        <script src='//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js' type='text/javascript'></script>
        <script src='http://podio.github.io/jquery-mentions-input/lib/jquery.events.input.js' type='text/javascript'></script>
        <script src='http://podio.github.io/jquery-mentions-input/lib/jquery.elastic.js' type='text/javascript'></script>
        <script src='http://podio.github.io/jquery-mentions-input/jquery.mentionsInput.js' type='text/javascript'></script>
        <script src='http://podio.github.io/jquery-mentions-input/assets/examples.js' type='text/javascript'></script>
        <script src='http://podio.github.io/jquery-mentions-input/assets/example2.js' type='text/javascript'></script>
        <!-- /theme JS files -->

        <script text="text/javascript">
            $(document).ready(function() {
                 @if(session('message') !== null)
                    new PNotify({
                        title: '{{ session("message")["header"] }}',
                        text: '{{ session("message")["body"] }}',
                        type: '{{ session("message")["alert"] }}',
                        addclass: 'bg-{{ session("message")["alert"] }}'
                    });
                @endif
                @if(!Auth::guest())
                var interval = 5000;  // 1000 = 1 second, 3000 = 3 seconds
                getNotifications();
                function getNotifications() {
                    $.ajax({
                            type: 'GET',
                            url: '{{ route("unreadNotifications", ["user" => Auth::user()->id]) }}',
                            data: $(this).serialize(),
                            dataType: 'json',
                            success: function (data) {
                                    $.each(data, function(n, elem) {
                                        console.log(elem.type);
                                        console.log(elem.avatar);   
                                        new PNotify({
                                    title: '',
                                    text: '<table width="width:100%"><tr><td><img src="' + elem.avatar +' " class="img-rounded white-bg"></td><td class="pl-10">' + elem.handle + elem.text + '</td></tr></table>',
                                    type: 'success',
                                    addclass: 'bg-primary alert',
                                    
                                });           
                                    });  
                            },
                            complete: function (data) {
                                    // Schedule the next
                                    setTimeout(getNotifications, interval);
                            }
                    });
                }

                setTimeout(getNotifications, interval);
                @endif

                $('textarea.mention').mentionsInput({
                  onDataRequest:function (mode, query, callback) {

                    if(query.length > 4){
                        var handle = query;
                        
                        
                    var data = function () {
                        var tmp = null;
                        $.ajax({
                            'async': false,
                            'type': "POST",
                            'global': false,
                            'dataType': 'json',
                            'url': "{{ route('handles') }}",
                            'data': { 
                                "handle": handle,
                                "_token": "{{ csrf_token() }}"
                              },
                            'success': function (data) {
                                tmp = data;
                            }
                        });
                        return tmp;
                    }();

                    data = _.filter(data, function(item) { return item.name.toLowerCase().indexOf(query.toLowerCase()) > -1 });

                    callback.call(this, data);
                    }

                    
                  }
                });

                $("body").on("click",".deletePost", function(){
                    if(confirm("Are you sure you want to delete this post? This cannot be undone.")){

                        var post = $(this).attr("data-post");

                        $.ajax({
                            'async': false,
                            'type': "DELETE",
                            'global': false,
                            'dataType': 'json',
                            'url': "{{ route('deletePost') }}",
                            'data': { 
                                "post": post,
                                "_token": "{{ csrf_token() }}"
                              },
                            'success': function (data) {
                                tmp = data;
                            }


                        });

                        $(this).parent().parent().parent().parent().parent().remove();
                    }
                });
            });
        </script>
        @yield('js')
    </body>
</html>