@extends('layouts.application')

@section('css')
    <style>
        
    </style>
@endsection

@section('content')
    <div class="col-md-8">
    @foreach($blogs as $blog)
        <!-- Blog layout #2 with image -->
        <div class="col-md-12 posts subject-{{ $blog->subject_id or '' }} shade-{{ $blog->shade_id or '' }}" data-subject="subject-{{ $blog->subject_id or '0' }}" data-shade="shade-{{ $blog->shade_id or '0' }}">
            <div class="panel panel-flat">
                <div class="panel-body">
                    <div class="thumb content-group">
                        @if(is_object($blog->media))
                            <img src="/uploads/media/{{ $blog->media->path }}" alt="" class="img-responsive">
                            
                        @endif
                        <div class="caption-overflow">
                            <span>
                                <a href="/podium/view/{{ $blog->id }}" class="btn btn-flat border-white text-white btn-rounded btn-icon"><i class="icon-arrow-right8"></i></a>
                            </span>
                        </div>
                    </div>
                    <h5 class="text-semibold content-group-sm">
                        <a href="#" class="text-default">{!! $blog->title !!}</a>
                    </h5>
                </div>

                <div class="panel-footer panel-footer-condensed">
                    <div class="heading-elements not-collapsible">
                        <ul class="list-inline list-inline-separate heading-text text-muted">
                            <li>By <a href="{{ route('user', ['user' => $blog->user_id]) }}" class="text-muted">{{ "@" . $blog->user->profile->handle }}</a></li>
                            <li>{{ convertTimeStamp($blog->created_at) }}</li>
                            @if(isset($blog->subject))
                                <li><span class="label bg-primary-400">{{ $blog->subject->title }}</span></li>
                            @endif
                            @if(isset($blog->shade))
                                <li><span class="label bg-indigo-400">{{ $blog->shade->title }}</span></li>
                            @endif
                        </ul>
                        <a href="/podium/view/{{ $blog->id }}" class="heading-text pull-right">Read more <i class="icon-arrow-right14 position-right"></i></a>
                        @if($blog->user_id == Auth::user()->id)
                        <a href="{{ route('edit-podium', ['blog' => $blog->id]) }}" class="heading-text pull-right">Edit <i class="fa fa-edit position-right"></i></a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <!-- /blog layout #2 with image -->
    @endforeach
    </div>
    <div class="col-md-4">
        <a href="{{ route('new-podium-post') }}"  class="btn btn-success btn-labeled btn-xlg" style="width:100%; margin-bottom: 20px;"><b><i class="fa fa-pencil-alt"></i></b> New Podium</a>
        <div class="panel panel-flat">
            <div class="panel-body">
                <h4>Filter by Subject</h4>
                <select name="select" class="form-control bg-primary-400 subject filter" id="subject">
                    <option value="subject-0">All Subjects</option>
                    @foreach($subjects as $subject)
                        <option value="subject-{{ $subject->id }}">{{ $subject->title }}</option>
                    @endforeach
                </select>

                <br>
                <h4>Filter by Shade</h4>
                <select name="select" class="form-control bg-indigo-400 shade filter" id="shade">
                    <option value="shade-0">All Shades</option>
                    @foreach($shades as $shade)
                        <option value="shade-{{ $shade->id }}">{{ $shade->title }}</option>
                    @endforeach
                </select>

            </div>
        </div>
        <div class="panel panel-flat col-md-12">
            <div class="panel-body">
                <img src="/assets/images/square_ad.jpg" style="width:100%;">
            </div>
        </div>
    </div>
@endsection

@section('js')
    <!-- Theme JS files -->
    <script type="text/javascript" src="/assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/editors/wysihtml5/wysihtml5.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/editors/wysihtml5/toolbar.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/editors/wysihtml5/parsers.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/editors/wysihtml5/locales/bootstrap-wysihtml5.ua-UA.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/notifications/jgrowl.min.js"></script>
    <script type="text/javascript" src="/assets/js/pages/editor_wysihtml5.js"></script>

    <script type="text/javascript">

    $(".filter").on("change", function(){
        var subject = $("#subject").val();
        var shade = $("#shade").val();

        filter(subject, shade);
    });

    $(".subject").on("change", function(){
        /*
        var val = $(this).val();
        if(val == "subject-0"){
            $(".posts").each(function(){
                $(this).show();
            });
        } else {
            $(".posts").each(function(){
                if(!$(this).hasClass(val)){
                    $(this).hide();
                }
            });
        }
        */
    });
    $(".shade").on("change", function(){
        /*
        var val = $(this).val();
        if(val == "shade-0"){
            $(".posts").each(function(){
                $(this).show();
            });
        } else {
            $(".posts").each(function(){
                if(!$(this).hasClass(val)){
                    $(this).hide();
                }
            });
        }
        */
    });

    function filter(subject, shade){
        // loop through posts


        $(".posts").each(function(){

            if(subject != "subject-0"){
                if(shade == "shade-0"){
                    if(!$(this).hasClass(subject)){
                        $(this).hide();
                    } else{
                        $(this).show();
                    }
                } else{
                    if(!$(this).hasClass(shade)){
                        $(this).hide();
                    } else {
                        $(this).show();
                    }
                }
            } else {
                if(shade == "shade-0"){
                    $(this).show();
                }
            }
            if(shade != "shade-0"){
                if(subject == "subject-0"){
                    if(!$(this).hasClass(shade)){
                        $(this).hide();
                    } else{
                        $(this).show();
                    }
                } else {
                    if(!$(this).hasClass(subject) || !$(this).hasClass(shade)){
                        $(this).hide();
                    } else {
                        $(this).show();
                    }
                }
            }
        });
    }
    </script>
@endsection
