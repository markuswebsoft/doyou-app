@extends('layouts.pod')

@section('content')
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-flat">
            <div class="panel-heading">
                <h6 class="panel-title">About the author<a class="heading-elements-toggle"><i class="icon-more"></i></a><a class="heading-elements-toggle"><i class="icon-more"></i></a></h6>
            </div>

            <div class="media panel-body no-margin">
                <div class="media-left">
                    
                </div>

                <div class="media-body">
                You do not have permission to view this pod. 
                </div>
            </div>
        </div>
    </div>
    <!--
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-flat">
            <div class="panel-body">
                <div class="col-md-6 text-left">
                    <h1><a href="{{ route('user', ['user' => $blog->user_id]) }}"><img src="{{ $blog->user->avatar() }}" class="comment-avatar img-circle" width="50"> {{ "@" . $blog->user->profile->handle }}</a></h1>
                </div>
                <div class="col-md-6 text-right">
                    <h1>{{ convertTimeStamp($blog->created_at) }}</h1>
                </div>

                <div class="col-md-12">
                    <h1>{!! $blog->title !!}</h1>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-10 col-md-offset-1">
        <div class="panel panel-flat">
            <div class="panel-body">
                
            </div>
        </div>
    </div>
    -->
@endsection