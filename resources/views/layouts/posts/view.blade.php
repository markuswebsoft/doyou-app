@extends('layouts.application')

@section('css')
	<style>
		.nav-tabs-highlight li a, .nav-tabs-highlight li a i{
            
        }
        .nav-tabs.nav-justified > li > a, .nav-tabs.nav-justified > li {
            border-bottom: 0;
            border-right: 0;
        }
        .tab-content-bordered .tab-content:not([class*=bg-]){
            background: none;
            border:0;
        }
        .tab-content, .tab-pane, .tab-content-bordered .tab-content > .has-padding{
            padding:0;
        }

        h5.trends{
            width:100%;
            margin:0;
            padding: 5px 10px;
            font-weight: bold;
            color:#1e80cf;
        }
        .trending table tr td{
            padding: 4px 12px;
        }
        .trending table tr td{
            border:0 !important;
        }
        .trending{
            max-height: 130px;
            overflow-y: scroll;
        }
        .uploader{
            max-width: 250px;
        }
        .uploader .filename{
            display:none;
        }
        span.quote{
            font-size:34px;
            color:silver;
            margin:0 20px;
        }
        .posts-timeline .panel-body, .posts-timeline .panel-footer{
            padding:0;
        }
        .posts-timeline .media{
            padding:10px;
        }
        .posts-timeline .content-group.thumb{
            min-height:200px;
        }
        .content-group.thumb{
            background-attachment: fixed;
            background-size: 100% 100%;
            background-repeat: no-repeat;
            background-position: center;
        }

        .comment-avatar{
            position: absolute;
            width: 30px;
            left: -12px;
            z-index: 3;
        }
        .post-info{
            border-bottom: 1px solid whitesmoke;
            margin-bottom:10px !important;
            padding-bottom: 10px !important;

        }
        li.post-info:before, li.post-info:after{
            border-width: 0 !important;
            border-left: 0 !important;
            border-right: 0 !important;
            border-color: white !important;
        }
        .content-group .btn-float i{
            font-size: 16px;
            font-weight: 300;
            width:15px;
            height:15px;
        }
        .content-group .btn-float:hover, .unlike, .unlike i{
            color:white !important;
            background: #F4511E !important;
        }
        .comment-list{
            padding-left: 13px;
        }
        .comment-reply{
            display: none;
        }
	</style>
@endsection

@section('content')
<div class="row">
	<div class="col-md-8 col-md-offset-2">
		<div class="panel panel-white">
		<div class="panel-body" style="background-image: url('/assets/images/text-post.jpg'); background-size: cover; background-position: left bottom; padding: 0;">
        <div class="row">
                    <div class="col-md-12" >
                        <ul class="media-list content-group-lg stack-media-on-mobile" style="margin-bottom:15px; padding:20px 20px 0 20px;">
                            <li class="media">
                                <div class="media-left">
                                    <a href="{{ route('user', ['id' => $post->user]) }}"><img src="{{ $post->user->avatar() }}" class="img-circle img-sm" alt=""></a>
                                </div>

                                <div class="media-body">
                                    <div class="media-heading">
                                        <a href="{{ route('user', ['id' => $post->user]) }}" class="text-semibold">{{ "@" . $post->user->profile->handle }}</a>
                                        <span class="media-annotation dotted">{{ convertTimeStamp($post->created_at) }}</span>
                                    </div>

                                    <ul class="list-inline list-inline-separate text-size-small">
                                        <li>114 <a href="#"><i class="icon-arrow-up22 text-success"></i></a><a href="#"><i class="icon-arrow-down22 text-danger"></i></a></li>
                                        @if(Auth::user()->id == $post->user_id)
                                            <li><a href="#">Edit</a></li>
                                        @endif
                                    </ul>
                                </div>
                            </li>
                        </ul>
                        @if(isset($post->featured))
                        <!-- 
                        <div class="thumb content-group" style="background-image:url('/uploads/media/{{ $post->featured() }}');">
                        </div>
                        -->
                        <div class="thumb content-group" style="margin-bottom: 0 !important;">
                            <img src="/uploads/media/{{ $post->featured() }}" style="border-radius: 0;">                       
                        </div>
                        @else
                        <div class="thumb content-group">
                            <h2 style="line-height: 200px;"><span class="quote"><i class="fa fa-quote-left"></i></span> {{ $post->content }} <span class="quote"><i class="fa fa-quote-right"></i></span></h2>
                        </div>
                        @endif
                        
                    </div>
                    </div>
                </div>
	      </div>
	</div>
</div>
<div class="row">
    <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-white">
        	<div class="panel-heading">
            	<h6 class="panel-title">Comments</h6>
	            <div class="heading-elements not-collapsible">
	            </div>
        	</div>
        	<div class="panel-body">
        		<div class="col-md-12">
                        <table>
			                <tr>
				                <td width="95%">
				                	<textarea type="text" class="form-control comment mention" style="border:none; border-radius: 0;" placeholder="Say something..." data-post="{{ $post->id }}" ></textarea>
				                </td>
				                <td>
				                	<button type="button" class="btn border-warning text-warning-600 btn-float btn-float-lg btn-rounded @if(Auth::user()->likesPost($post->id)) unlike @else like @endif" data-post="{{ $post->id }}"><i class="far fa-heart"></i></button>
				                </td>
			                </tr>
                		</table>
                		<div class="comment-list">
                        <ul class="media-list content-group-lg stack-media-on-mobile">
                            @foreach($post->comments as $comment)
                            <li class="media">
                                <div class="media-left">
                                    <a href="{{ route('user', ['id' => $comment->user_id]) }}"><img src="{{ $comment->user->avatar() }}" class="img-circle img-sm" alt=""></a>
                                </div>

                                <div class="media-body">
                                    <div class="media-heading">
                                        <a href="{{ route('user', ['id' => $comment->user_id]) }}" class="text-semibold">{{ "@" . $comment->user->profile->handle }}</a>
                                        <span class="media-annotation dotted">{{ convertTimeStamp($comment->created_at) }}</span>
                                    </div>

                                    <p>{{ $comment->comment }}</p>

                                    <ul class="list-inline list-inline-separate text-size-small">
                                        <li>114 <a href="#"><i class="icon-arrow-up22 text-success"></i></a></li>
                                        <li><a href="javascript:;" class="insert-reply">Reply</a></li>
                                        @if(Auth::user()->id == $comment->user_id)
                                            <li><a href="#">Edit</a></li>
                                        @endif
                                    </ul>
                                    <div class="media comment-reply">
                                        <div class="media-left" style="display:inline-block; width:93%;">
                                            <textarea class="form-control reply" data-post="{{ $post->id }}" data-comment="{{ $comment->id }}"></textarea>
                                        </div>
                                        <div class="media-left text-center" style="display: inline;">
                                        <button type="button" class="btn border-info text-info btn-icon btn-flat btn-sm btn-rounded"><i class="icon-git-pull-request"></i></button>
                                            
                                        </div>
                                    </div>
                                    @foreach($comment->replies() as $reply)
                                    <div class="media replies-{{ $comment->id }}">
                                        
                                            <div class="media-left">
                                                <a href="#"><img src="{{ $reply->user->avatar() }}" class="img-circle img-sm" alt=""></a>
                                            </div>

                                            <div class="media-body">
                                                <div class="media-heading">
                                                    <a href="#" class="text-semibold">Jack Cooper</a>
                                                    <span class="media-annotation dotted">10 minutes ago</span>
                                                </div>

                                                <p>{{ $reply->comment }}</p>

                                                <ul class="list-inline list-inline-separate text-size-small">
                                                    <li>67 <a href="#"><i class="icon-arrow-up22 text-success"></i></a><a href="#"><i class="icon-arrow-down22 text-danger"></i></a></li>
                                                    @if(Auth::user()->id == $comment->user_id)
                                                        <li><a href="#">Edit</a></li>
                                                    @endif
                                                </ul>
                                            </div>
                                        
                                    </div>
                                    @endforeach
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
        	</div>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script type="text/javascript">
        $("body").on("keyup", ".comment", function(event){
            if (event.keyCode === 13) {
                var today = moment().format("MMM. D, Y"); 
                var comment = $(this).val();
                var post = $(this).attr("data-post");
                if(comment != ""){
                    $.post( "{{ route('comment') }}", { post: post, comment: comment, _token: "{{ CSRF_TOKEN() }}"}, function(data, status){

                    });
                    $(this).parent().parent().parent().parent().parent().parent().find(".list-feed-comments").prepend('<li class="border-primary-300"><a href="{{ route("user", ["id" => Auth::user()->id]) }}"><img src="{{ Auth::user()->avatar() }}" class="comment-avatar img-circle"></a><a href="{{ route("user", ["id" => Auth::user()->id]) }}">{{"@" . Auth::user()->profile->handle }}</a> ' + comment + '<br>' + today + '</li>');
                    $(this).val("");
                }
            }
        });
        $("body").on("keyup", ".reply", function(event){
            if (event.keyCode === 13) {
                var today = moment().format("MMM. D, Y"); 
                var comment = $(this).val();
                var post = $(this).attr("data-post");
                var reply = $(this).attr("data-comment");
                if(comment != ""){
                    $.post( "{{ route('commentReply') }}", { post: post, comment: comment, reply: reply, _token: "{{ CSRF_TOKEN() }}"}, function(data, status){

                    });
                    $(this).parent().parent().parent().parent().parent().parent().find(".list-feed-comments").prepend('<li class="border-primary-300"><a href="{{ route("user", ["id" => Auth::user()->id]) }}"><img src="{{ Auth::user()->avatar() }}" class="comment-avatar img-circle"></a><a href="{{ route("user", ["id" => Auth::user()->id]) }}">{{"@" . Auth::user()->profile->handle }}</a> ' + comment + '<br>' + today + '</li>');
                    $(this).val("");
                }
            }
        });
        $("body").on("click", ".like", function(event){
            var post = $(this).attr("data-post");
            $.ajax({
                type: "GET",
                url: "{{ route('like', ['post' => null]) }}/" + post,
                async: false
            });
            $(this).removeClass("like");
            $(this).addClass("unlike");
        });
        $("body").on("click", ".unlike", function(event){
            var post = $(this).attr("data-post");
            $.ajax({
                type: "GET",
                url: "{{ route('unlike', ['post' => null]) }}/" + post,
                async: false
            });
            $(this).removeClass("unlike");
            $(this).addClass("like");
        });

        $("body").on("click", ".insert-reply", function(event){
            $(this).parent().parent().parent().find(".comment-reply").toggle();
            $(this).text($(this).text() == "Reply" ? "Cancel" : "Reply");
        });
    </script>
@endsection