@extends('layouts.application')

@section('css')
    <style>
        fieldset{
            min-height: 300px;
        }

        .preview{
            width: 200px;
            height: 200px;
            text-align: center;
            display: block;
            margin: 0 auto;
        }
        .preview img{
            margin:0 auto;
            text-align:center;
            width:100%;
        }
    </style>
@endsection

@section('content')
    <!-- Wizard with validation -->
    
        <form method="POST" action="{{ route('editProfile') }}" enctype="multipart/form-data">
        <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Select Username <span class="text-danger">*</span></h6>
        </div>
        <div class="panel-body">
            @csrf
                <div class="row">
                     <label></label>
                     Remember to make it
                     <ul>
                        <li>between 4 and 12 characters long</li>
                     </ul>
                     <div class="form-group has-feedback has-feedback-left">
                        <input type="text" class="form-control input-xlg required" placeholder="username" id="username" required="" minlength="4" maxlength="12" name="handle" value="{{ $profile->handle }}">
                        <div class="form-control-feedback">
                            <i class="icon-at">@</i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="panel panel-white">
                    <div class="panel-heading">
                        <h6 class="panel-title">Current Avatar</h6>
                    </div>
                    <div class="panel-body">
                        @if(isset($avatar->path))
                        <a href="/uploads/avatars/{{ $avatar->path }}" target="_blank"><img src="/uploads/avatars/{{ $avatar->path }}" style="width: 100px; height: 100px;"></a>
                        <br><br>
                        @endif
                        <div class="form-group has-feedback has-feedback-left">
                            <input type="file" name="avatar" class="file-styled" id="avatar">
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="panel panel-white">
                    <div class="panel-heading">
                        <h6 class="panel-title">Replace With</h6>
                    </div>
                    <div class="panel-body">
                        <div id="avatarPreview" class="preview"></div>
                    </div>
                </div>
            </div>
        </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="panel panel-white">
                            <div class="panel-heading">
                                <h6 class="panel-title">Current Banner</h6>
                            </div>
                            <div class="panel-body">
                                @if(isset($banner->path))
                                <a href="/uploads/banners/{{ $banner->path }}" target="_blank"><img src="/uploads/banners/{{ $banner->path }}" style="height: 100px;"></a>
                                <br><br>
                                @endif
                                <div class="form-group has-feedback has-feedback-left">
                                    <input type="file" name="banner" class="file-styled" id="banner">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="panel panel-white">
                        <div class="panel-heading">
                            <h6 class="panel-title">Replace With</h6>
                        </div>
                        <div class="panel-body">
                                

                                    <div id="bannerPreview" class="preview"></div>
                                </div>
                        </div>
                    </div>
                </div>
        <div class="panel panel-white">
        <div class="panel-heading">
            <h6 class="panel-title">Edit Your Profile</h6>
        </div>
        <div class="panel-body">
                <div class="row">
                    <label>Bio: Talk about where you're from, languages you speak, your pets, and even if you're team vegan or carnivore.</label>
                     <div class="form-group">
                        <textarea class="form-control" name="bio" maxlength="255">{{ $profile->bio }}</textarea>
                    </div>
                </div>
                <div class="row">
                    <label>Website:</label>
                     <div class="form-group has-feedback has-feedback-left">
                        <input type="text" class="form-control input-xlg" name="website" placeholder="www.yourblog.com" value="{{ $profile->website }}">
                        <div class="form-control-feedback">
                            <i class="fa fa-globe" style="font-size: 16px;"></i>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label>Where are you located:</label>
                     <div class="form-group has-feedback has-feedback-left">
                        <input type="text" class="form-control input-xlg" name="location" placeholder="City, State, Country" value="{{ $profile->location }}">
                        <div class="form-control-feedback">
                            <i class="icon-map"></i>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label>What's your favorite color?</label>
                    @if(isset($profile->color))
                    <div style="margin-bottom:20px; width: 50px; height: 50px; border: 1px dashed black; background: {{ $profile->color }}">
                    </div>
                    @endif
                    <div class="form-group has-feedback has-feedback-left">
                        <select class="form-control" name="color">
                        <option value="">Pick one :)</option>
                            <option value="Red" @if($profile->color == "Red") selected="" @endif>Red</option>
                            <option value="Orange" @if($profile->color == "Orange") selected="" @endif>Orange</option>
                            <option value="Yellow" @if($profile->color == "Yellow") selected="" @endif>Yellow</option>
                            <option value="Green" @if($profile->color == "Green") selected="" @endif>Green</option>
                            <option value="Blue" @if($profile->color == "Blue") selected="" @endif>Blue</option>
                            <option value="Purple" @if($profile->color == "Purple") selected="" @endif>Purple</option>
                            <option value="Black" @if($profile->color == "Black") selected="" @endif>Black</option>
                            <option value="White" @if($profile->color == "White") selected="" @endif>White</option>
                            <option value="Pink" @if($profile->color == "Pink") selected="" @endif>Pink</option>
                            <option value="Silver" @if($profile->color == "Silver") selected="" @endif>Silver</option>
                            <option value="None" @if($profile->color == "Red") selected="" @endif>I don't have one :(</option>
                        </select>
                    </div>
                </div>
                <div class="row">
                    <label>When was the world blessed with your birth?</label>
                    <div class="form-group">
                        <input type="date" name="dob" class="form-control" value="{{ $profile->dob }}">
                    </div>
                </div>
                <div class="row">
                    <input type="submit" name="submit" class="btn btn-success" value="Save Changes">
                </div>
            </form>
        </div>
    </div>
    <!-- /wizard with validation -->
@endsection

@section('js')
    <script type="text/javascript" src="/assets/js/plugins/media/validate.js"></script>
    <script type="text/javascript">
    jQuery(function($) {
        $('#username').on('keyup', function() {
            var username = $(this).val();
            if(username != ""){
                $.post( "{{ route('confirmUsername') }}", { username: username, _token: "{{ CSRF_TOKEN() }}"}, function(data, status){
                    if(data == "ok"){
                        $('#username').parent().addClass("has-success");
                        $('#username').parent().removeClass("has-error");
                    } else{
                        $('#username').parent().addClass("has-error");
                        $('#username').parent().removeClass("has-success");
                    }
                });
            }
        });
    });

    </script>
@endsection
