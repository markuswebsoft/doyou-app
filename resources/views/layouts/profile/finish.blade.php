@extends('layouts.application')

@section('css')
    <style>
        fieldset{
            min-height: 300px;
        }
    </style>
@endsection

@section('content')
    <!-- Wizard with validation -->
                    <div class="panel panel-white">
                        <div class="panel-heading">
                            <h6 class="panel-title">Finish Your Profile</h6>
                            <div class="heading-elements">
                                <ul class="icons-list">
                                    <li><a data-action="collapse"></a></li>
                                    <li><a data-action="reload"></a></li>
                                    <li><a data-action="close"></a></li>
                                </ul>
                            </div>
                        </div>

                        <form class="steps-validation" method="POST" action="{{ route('finishProfile') }}" enctype="multipart/form-data">
                            @csrf
                            <h6>Personalize</h6>
                            <fieldset>
                                <div class="row">
                                     <label>Select Username: <span class="text-danger">*</span></label>
                                     Remember to make it
                                     <ul>
                                        <li>between 4 and 12 characters long</li>
                                     </ul>
                                     <div class="form-group has-feedback has-feedback-left">
                                        <input type="text" class="form-control input-xlg required" placeholder="username" id="username" required="" minlength="4" maxlength="12" name="handle">
                                        <div class="form-control-feedback">
                                            <i class="icon-at">@</i>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label>Profile Picture:</label>
                                    <div class="form-group has-feedback has-feedback-left">
                                        <input type="file" name="avatar" class="file-styled">
                                    </div>
                                </div>
                                <div class="row">
                                    <label>Profile Background:</label>
                                    <div class="form-group has-feedback has-feedback-left">
                                        <input type="file" name="banner" class="file-styled">
                                    </div>
                                </div>
                            </fieldset>

                            <h6>General</h6>
                            <fieldset>
                                <div class="row">
                                    <label>Bio:</label>
                                     <div class="form-group has-feedback has-feedback-left">
                                        <textarea class="form-control" name="bio" maxlength="255"></textarea>
                                    </div>
                                </div>
                                <div class="row">
                                    <label>Website:</label>
                                     <div class="form-group has-feedback has-feedback-left">
                                        <input type="text" class="form-control input-xlg" name="website" placeholder="www.yourblog.com">
                                        <div class="form-control-feedback">
                                            <i class="fa fa-globe" style="font-size: 16px;"></i>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <label>Where are you located: <span class="text-danger">*</span></label>
                                     <div class="form-group has-feedback has-feedback-left">
                                        <input type="text" class="form-control input-xlg" name="location" placeholder="City, State, Country">
                                        <div class="form-control-feedback">
                                            <i class="icon-map"></i>
                                        </div>
                                    </div>
                                </div>
                            </fieldset>
                        </form>
                    </div>
                    <!-- /wizard with validation -->
@endsection

@section('js')
    <script type="text/javascript">
    jQuery(function($) {
        $('#username').on('keyup', function() {
            var username = $(this).val();
                if(username != ""){
                    $.post( "{{ route('confirmUsername') }}", { username: username, _token: "{{ CSRF_TOKEN() }}"}, function(data, status){
                        if(data == "ok"){
                            $('#username').parent().addClass("has-success");
                            $('#username').parent().removeClass("has-error");
                        } else{
                            $('#username').parent().addClass("has-error");
                            $('#username').parent().removeClass("has-success");
                        }
                    });
                }
        });
    });

    </script>
@endsection
