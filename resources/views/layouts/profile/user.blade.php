@extends('layouts.application')

@section('css')
    <style type="text/css">
    .content, {
        padding:0 !important;
    }
        .profile-cover{
            max-height: 350px;
            overflow:hidden;
        }
        .profile-thumb{
            position: absolute;
            z-index: 5;
            margin-top: -50px;
            width:100px;
            left: 50%;
            margin-left: -100px;
        }
        .profile-thumb img{
            width:100px;
            height:100px;
        }
        .profile-portlet{
            width: 300px;
            position: absolute;
            top:175px;
            margin-left:50px;
            background: white;
            box-shadow: 0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24);
        }
        .profile-portlet .content{
            padding:15px;
        }
        .profile-portlet .handle{
            width: 100%; 
            text-align: center;
            padding-top:0;
            margin-top:0 !important;
        }
        .nav-tabs-highlight li a, .nav-tabs-highlight li a i{
            
        }
        .nav-tabs.nav-justified > li > a, .nav-tabs.nav-justified > li {
            border-bottom: 0;
            border-right: 0;
        }
        .tab-content-bordered .tab-content:not([class*=bg-]){
            background: none;
            border:0;
        }
        .tab-content, .tab-pane, .tab-content-bordered .tab-content > .has-padding, .posts-timeline .panel-body, .posts-timeline .panel-footer{
            padding:0;
        }
        .search-tab{
            min-width: 190px;
            max-width: inherit !important;
            vertical-align: middle;
            background: white;
            padding:10px
            border-bottom:1px solid silver;
            height:51px;
        }
        .nav-tabs.nav-justified > li.search-tab{
            float:right;
            border-bottom: 1px solid #ddd;
            border-left: 1px solid #ddd;
            border-bottom: 1px solid #ddd;
            border-top: 1px solid #ddd;
        }
        .search-tab .form-control{
            border: none;
            height:51px;
        }
        .search-tab .input-group-addon{
                padding: 16px 17px;
                font-size: 18px;
        }
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
            color: #2196F3;
        }
        .content.profile{
            position: absolute;
            right:0;
            padding-right:0;
        }
        .tab-pane{
            position: absolute;
            right:0;
            width:100%;
        }
        #tab-posts{
            
        }
        .comment-avatar{
            position: absolute;
            width: 30px;
            left: -12px;
            z-index: 3;
        }
        .post-info{
            border-bottom: 1px solid whitesmoke;
            margin-bottom:10px !important;
            padding-bottom: 10px !important;

        }
        li.post-info:before, li.post-info:after{
            border-width: 0 !important;
            border-left: 0 !important;
            border-right: 0 !important;
            border-color: white !important;
        }
        .content-group .btn-float{
            padding:10px;
        }
        .content-group .btn-float i{
            font-size: 16px;
            font-weight: 300;
        }
        .content-group .btn-float:hover, .unlike, .unlike i{
            color:white !important;
            background: #F4511E !important;
        }
        .comment-list{
            max-height: 226px;
            overflow-y: auto;
            padding-left: 13px;
        }
        .uploader .action{
            z-index: 0;
        }
        .view-post{
            margin-top:20px;
            padding:0;
        }
        .view-post:hover{
            cursor:pointer;
        }
        @media (min-width: 769px){
.nav-tabs.nav-justified > li {
    width:130px;
    font-size:14px;
}
}
        .preview.shown{
            width: 200px;
            height: 200px;
        }

        .preview{
            text-align: center;
            display: block;
            margin: 0 auto;
        }
        .preview img{
            margin:0 auto;
            text-align:center;
            width:100%;
        }
        .file-styled{
            width:100%;
        }
        #reset{
            display: none;
        }
        .list-feed-comments{
            background: white !important;
        }
    </style>
@endsection

@section('content')
    <div class="profile-cover">
        <div class="profile-cover-img" style="background-size:cover; background-image: url('{{ Auth::user()->banner() }}')"></div>
        <div class="media">
            <div class="media-left">
                
            </div>

            <div class="media-body">
                
            </div>
            @if($owner)
            <div class="media-right media-middle">
                <ul class="list-inline list-inline-condensed no-margin-bottom text-nowrap">
                    <li><a href="{{ route('view-edit-profile', ['id' => Auth::user()->id]) }}" class="btn btn-default"><i class="fa fa-cogs"></i> Edit Profile</a></li>
                </ul>
            </div>
            @endif
        </div>
    </div>
    <div class="profile-portlet">
        <img src="{{ $profile->user->avatar() }}" width="100%">
        <div class="content">
            <h2 class="handle text-center">{{ "@" . $profile->handle }}</h2>
            <div class="btn-group btn-group-justified">
            @if(!$owner)

                <div class="btn-group" style="width: 2px;">
                    <a href="{{ route('conversation', ['user' => $profile->user_id ]) }}" class="btn bg-slate-700" style=""><i class="fa fa-comment"></i></a>
                </div>
                <div class="btn-group">
                    @if(!Auth::user()->imFollowingThem($profile->user_id) )
                        <button type="button" class="btn bg-slate-700" id="follow"><i class="fa fa-user"></i> Follow</button>
                        @else
                        <button type="button" class="btn bg-success-700" id="unfollow"><i class="fa fa-user"></i> Following</button>
                    @endif
                </div>

                <div class="btn-group">
                    @if(!Auth::user()->imConnectedToThem($profile->user_id) )
                        <button type="button" class="btn bg-slate-700" id="connect"><i class="fa fa-hands-helping"></i> Connect</button>
                    @elseif(Auth::user()->imConnectedToThem($profile->user_id) === true)
                        <button type="button" class="btn bg-primary-700" id="disconnect"><i class="fa fa-hands-helping"></i> Connected</button>
                    @elseif(Auth::user()->imConnectedToThem($profile->user_id) == "pending")
                        <button type="button" class="btn bg-slate-700" id="disconnect"><i class="fa fa-hands-helping"></i> Pending</button>
                    @endif
                </div>
            @endif
            </div>
            <br>
            <p><b>ABOUT</b></p>
            <p>{{ $profile->bio }}</p>
            <p><b>Location</b></p>
            <p>{{ $profile->location }}</p>
            <p><b>Website</b></p>
            <p>{{ $profile->website }}</p>
            <p><b>Pods</b></p>
            <p>No Pods Joined</p>
            <p>
            <a href="#" class="btn btn-white btn-flat" style="width:100%;">Learn More</a>
            </p>
        </div>
    </div>
    <div class="col-md-offset-4 content" style="padding:0">
        <div class="tab-content-bordered content-group">
            <ul class="nav nav-tabs nav-tabs-highlight nav-lg nav-justified">
                <li class="active"><a href="#tab-posts" data-toggle="tab">Posts</a></li>
                <li><a href="#tab-mentions" data-toggle="tab">Projects</a></li>
                <li><a href="#tab-topics" data-toggle="tab">Network</a></li>
                <li><a href="#tab-questions" data-toggle="tab">Favorites</a></li>
            </ul>
            <div class="tab-content col-md-12">
                
                <div class="tab-pane has-padding active" id="tab-posts">
                    @if($owner)
                <!-- content draft -->

                <!-- Blog layout #4 with image -->
                        <div class="panel panel-flat">
                            <div class="panel-body" style="padding:0;">
                                <div class="row">
                                    <div class="col-md-12" style="padding:20px">
                                        @include('layouts.publish')
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="posts-timeline">
                        @foreach($posts as $post)
                            @include('layouts.posts')   
                        @endforeach
                    </div>
                </div>
                <div class="tab-pane has-padding" id="tab-mentions">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="col-md-8">
                                <div class="thumb content-group">
                                    <img src="/assets/images/demo/images/blog2.jpg" alt="" class="img-responsive">
                                    <div class="caption-overflow">
                                        <span>
                                            <a href="blog_single.html" class="btn btn-flat border-white text-white btn-rounded btn-icon"><i class="icon-arrow-right8"></i></a>
                                        </span>
                                    </div>
                                </div>
                                <ul class="list-inline heading-text">
                                    <li><a href="#" class="text-default"><i class="icon-comment position-left"></i>12</a></li>
                                    <li><a href="#" class="text-default"><i class="icon-heart6 text-pink position-left"></i> 281</a></li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <div class="media">
                                    <a href="#" class="media-left"><img src="/assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                                    <div class="media-body">
                                        <span class="media-heading text-semibold">{{ "@" . $profile->handle }}</span>
                                        <div class="text-size-mini text-muted">
                                            <ul class="list-inline list-inline-separate text-muted content-group">
                                                <li>July 5th, 2016</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p>When suspiciously goodness labrador understood rethought yawned grew piously endearingly inarticulate oh goodness jeez trout distinct hence cobra despite. (read more)</p>
                                    <div class="panel-body">
                                        <ul class="media-list chat-list content-group">
                                            <li class="media reversed">
                                                <div class="media-body">
                                                    <div class="media-content">Satisfactorily strenuously while sleazily</div>
                                                    <span class="media-annotation display-block mt-10">2 hours ago</span>
                                                </div>

                                                <div class="media-right">
                                                    <a href="assets/images/demo/images/3.png">
                                                        <img src="/assets/images/demo/users/face1.jpg" class="img-circle img-md" alt="">
                                                    </a>
                                                </div>
                                            </li>

                                            <li class="media">
                                                <div class="media-left">
                                                    <a href="assets/images/demo/images/3.png">
                                                        <img src="/assets/images/demo/users/face11.jpg" class="img-circle img-md" alt="">
                                                    </a>
                                                </div>

                                                <div class="media-body">
                                                    <div class="media-content">Grunted smirked and grew.</div>
                                                    <span class="media-annotation display-block mt-10">13 minutes ago</span>
                                                </div>
                                            </li>

                                            <li class="media reversed">
                                                <div class="media-body">
                                                    <div class="media-content"><i class="icon-menu display-block"></i></div>
                                                </div>

                                                <div class="media-right">
                                                    <a href="assets/images/demo/images/3.png">
                                                        <img src="/assets/images/demo/users/face1.jpg" class="img-circle img-md" alt="">
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>

                                        <input name="enter-message" class="form-control content-group" placeholder="Write a comment">

                                        <div class="row">
                                            <div class="col-xs-6">
                                                <ul class="icons-list icons-list-extended mt-10">
                                                    <li><a href="#" data-popup="tooltip" data-container="body" title="" data-original-title="Send photo"><i class="icon-file-picture"></i></a></li>
                                                    <li><a href="#" data-popup="tooltip" data-container="body" title="" data-original-title="Send video"><i class="icon-file-video"></i></a></li>
                                                    <li><a href="#" data-popup="tooltip" data-container="body" title="" data-original-title="Send file"><i class="icon-file-plus"></i></a></li>
                                                </ul>
                                            </div>

                                            <div class="col-xs-6 text-right">
                                                <button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-right"><b><i class="icon-circle-right2"></i></b> Send</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane has-padding" id="tab-topics">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="col-md-12">
                                <!-- Media library -->
                        <table class="table table-striped table-lg">
                            <thead>
                                <tr>
                                    <th></th>
                                    <th>Username</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                            @foreach($network as $person)
                                <tr>
                                    <td width="100">
                                        <a href="{{ route('user', ['user' => $person['connected']['profile']['user_id']]) }}" data-popup="lightbox">
                                            <img src="{{ getAvatar($person['connected']['profile']['user_id']) }}" alt="" class="img-rounded img-preview">
                                        </a>
                                    </td>
                                    <td><a href="{{ route('user', ['user' => $person['connected']['profile']['user_id']]) }}">{{ "@" }}{{ $person['connected']['profile']['handle'] }}</a></td>
                                    <td></td>
                                </tr>
                            @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th>Username</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    <!-- /media library -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane has-padding" id="tab-questions">
                    <div class="panel panel-flat">
                        <div class="panel-body">
                            <div class="col-md-8">
                                <div class="thumb content-group">
                                    <img src="/assets/images/demo/images/blog5.jpg" alt="" class="img-responsive">
                                    <div class="caption-overflow">
                                        <span>
                                            <a href="blog_single.html" class="btn btn-flat border-white text-white btn-rounded btn-icon"><i class="icon-arrow-right8"></i></a>
                                        </span>
                                    </div>
                                </div>
                                <ul class="list-inline heading-text">
                                    <li><a href="#" class="text-default"><i class="icon-comment position-left"></i>12</a></li>
                                    <li><a href="#" class="text-default"><i class="icon-heart6 text-pink position-left"></i> 281</a></li>
                                </ul>
                            </div>
                            <div class="col-md-4">
                                <div class="media">
                                    <a href="#" class="media-left"><img src="/assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                                    <div class="media-body">
                                        <span class="media-heading text-semibold">{{ "@" . $profile->handle }}</span>
                                        <div class="text-size-mini text-muted">
                                            <ul class="list-inline list-inline-separate text-muted content-group">
                                                <li>July 5th, 2016</li>
                                            </ul>
                                        </div>
                                    </div>
                                    <p>When suspiciously goodness labrador understood rethought yawned grew piously endearingly inarticulate oh goodness jeez trout distinct hence cobra despite. (read more)</p>
                                    <div class="panel-body">
                                        <ul class="media-list chat-list content-group">
                                            <li class="media reversed">
                                                <div class="media-body">
                                                    <div class="media-content">Satisfactorily strenuously while sleazily</div>
                                                    <span class="media-annotation display-block mt-10">2 hours ago</span>
                                                </div>

                                                <div class="media-right">
                                                    <a href="assets/images/demo/images/3.png">
                                                        <img src="/assets/images/demo/users/face1.jpg" class="img-circle img-md" alt="">
                                                    </a>
                                                </div>
                                            </li>

                                            <li class="media">
                                                <div class="media-left">
                                                    <a href="assets/images/demo/images/3.png">
                                                        <img src="/assets/images/demo/users/face11.jpg" class="img-circle img-md" alt="">
                                                    </a>
                                                </div>

                                                <div class="media-body">
                                                    <div class="media-content">Grunted smirked and grew.</div>
                                                    <span class="media-annotation display-block mt-10">13 minutes ago</span>
                                                </div>
                                            </li>

                                            <li class="media reversed">
                                                <div class="media-body">
                                                    <div class="media-content"><i class="icon-menu display-block"></i></div>
                                                </div>

                                                <div class="media-right">
                                                    <a href="assets/images/demo/images/3.png">
                                                        <img src="/assets/images/demo/users/face1.jpg" class="img-circle img-md" alt="">
                                                    </a>
                                                </div>
                                            </li>
                                        </ul>

                                        <input name="enter-message" class="form-control content-group" placeholder="Write a comment">

                                        <div class="row">
                                            <div class="col-xs-6">
                                                <ul class="icons-list icons-list-extended mt-10">
                                                    <li><a href="#" data-popup="tooltip" data-container="body" title="" data-original-title="Send photo"><i class="icon-file-picture"></i></a></li>
                                                    <li><a href="#" data-popup="tooltip" data-container="body" title="" data-original-title="Send video"><i class="icon-file-video"></i></a></li>
                                                    <li><a href="#" data-popup="tooltip" data-container="body" title="" data-original-title="Send file"><i class="icon-file-plus"></i></a></li>
                                                </ul>
                                            </div>

                                            <div class="col-xs-6 text-right">
                                                <button type="button" class="btn bg-teal-400 btn-labeled btn-labeled-right"><b><i class="icon-circle-right2"></i></b> Send</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <!-- Theme JS files -->
    <script type="text/javascript" src="/assets/js/plugins/media/fancybox.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/forms/styling/uniform.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/forms/selects/select2.min.js"></script>
    <script type="text/javascript" src="/assets/js/plugins/media/validate.js"></script>
    <script type="text/javascript" src="/assets/js/core/app.js"></script>
    <script text="text/javascript">
        $(document).ready(function() {

        $("body").on("click", "#follow", function(){
            $.ajax({
                type: "GET",
                url: "{{ route('follow', ['user' => $profile->user_id]) }}",
                async: false,
                success: function(){
                    
                }
            });
            $(this).removeClass("bg-slate-700");
            $(this).addClass("bg-success-700");
            $(this).html("<i class='fa fa-user'></i> Following");
            $(this).attr("id", "unfollow");
        });

        $("body").on("click", "#unfollow", function(){
            $.ajax({
                type: "GET",
                url: "{{ route('unfollow', ['user' => $profile->user_id]) }}",
                async: false,
                success: function(){
                    
                }

            });
            $(this).removeClass("bg-success-700");
            $(this).addClass("bg-slate-700");
            $(this).html("<i class='fa fa-user'></i> Follow");
            $(this).attr("id", "follow");
        });

        $("body").on("click", "#connect", function(){
            $.ajax({
                type: "GET",
                url: "{{ route('connect', ['user' => $profile->user_id]) }}",
                async: false,
                success: function(){
                    
                }
            });
            $(this).html('<i class="fa fa-hands-helping"></i> Pending');
            $(this).attr("id", "disconnect");
        });
        
        $("body").on("click", "#disconnect", function(){
            $.ajax({
                type: "GET",
                url: "{{ route('disconnect', ['user' => $profile->user_id]) }}",
                async: false,
                success: function(){
                    
                }
            });
            $(this).html('<i class="fa fa-hands-helping"></i> Connect');
            $(this).attr("id", "connect");
        });
        $("body").on("keyup", ".comment", function(event){
            if (event.keyCode === 13) {
                var today = moment().format("MMM. D, Y"); 
                var comment = $(this).val();
                var post = $(this).attr("data-post");
                if(comment != ""){
                    $.post( "{{ route('comment') }}", { post: post, comment: comment, _token: "{{ CSRF_TOKEN() }}"}, function(data, status){

                    });
                    $(this).parent().parent().parent().parent().parent().parent().find(".list-feed-comments").prepend('<li class="border-primary-300"><a href="{{ route("user", ["id" => Auth::user()->id]) }}"><img src="{{ Auth::user()->avatar() }}" class="comment-avatar img-circle"></a><a href="{{ route("user", ["id" => Auth::user()->id]) }}">{{"@" . Auth::user()->profile->handle }}</a> ' + comment + '<br>' + today + '</li>')
                    $(this).val("");
                }
            }
        });
        $("body").on("click", ".like", function(event){
            var post = $(this).attr("data-post");
            $.ajax({
                type: "GET",
                url: "{{ route('like', ['post' => null]) }}/" + post,
                async: false
            });
            $(this).removeClass("like");
            $(this).addClass("unlike");
        });
        $("body").on("click", ".unlike", function(event){
            var post = $(this).attr("data-post");
            $.ajax({
                type: "GET",
                url: "{{ route('unlike', ['post' => null]) }}/" + post,
                async: false
            });
            $(this).removeClass("unlike");
            $(this).addClass("like");
        });

        $("body").on("click", ".view-post", function(){
            window.location.href = $(this).attr("data-url");
        });
    });
    </script>
@endsection
