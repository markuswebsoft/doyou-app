@extends('layouts.application')

@section('css')
    <style>

        .nav-tabs-highlight li a, .nav-tabs-highlight li a i{
            
        }
        .nav-tabs.nav-justified > li > a, .nav-tabs.nav-justified > li {
            border-bottom: 0;
            border-right: 0;
        }
        .tab-content-bordered .tab-content:not([class*=bg-]){
            background: none;
            border:0;
        }
        .tab-content, .tab-pane, .tab-content-bordered .tab-content > .has-padding{
            padding:0;
        }
        .search-tab{
            width: 650px !important;
            max-width: inherit !important;
            vertical-align: middle;
            background: white;
            padding:10px
            border-bottom:1px solid silver;
            height:51px;
        }
        .nav-tabs.nav-justified > li.search-tab{
            float:right;
            border-bottom: 1px solid #ddd;
            border-left: 1px solid #ddd;
            border-bottom: 1px solid #ddd;
            border-top: 1px solid #ddd;
        }
        .search-tab .form-control{
            border: none;
            height:51px;
        }
        .search-tab .input-group-addon{
                padding: 16px 17px;
                font-size: 18px;
        }
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
            color: #2196F3;
        }
        h6.trends{
            width:100%;
            margin:0;
            padding: 5px 0;
            font-weight: bold;
            color:#1e80cf;
        }
        .trending table tr td{
            padding: 4px 12px;
        }
        .trending table tr td{
            border:0 !important;
        }
        .trending{
            max-height: 300px;
            overflow-y: scroll;
            padding-bottom:20px;
        }

        span.quote{
            font-size:34px;
            color:silver;
            margin:0 20px;
        }
        .posts-timeline .panel-body, .posts-timeline .panel-footer{
            padding:0;
        }
        .posts-timeline .media{
            padding:10px;
        }
        .posts-timeline .content-group.thumb{
            min-height:200px;
        }
        .content-group.thumb{
            background-attachment: fixed;
            background-size: 100% 100%;
            background-repeat: no-repeat;
            background-position: center;
        }

        .comment-avatar{
            position: absolute;
            width: 30px;
            left: -12px;
            z-index: 3;
        }
        .post-info{
            border-bottom: 1px solid whitesmoke;
            margin-bottom:10px !important;
            padding-bottom: 10px !important;

        }
        li.post-info:before, li.post-info:after{
            border-width: 0 !important;
            border-left: 0 !important;
            border-right: 0 !important;
            border-color: white !important;
        }
        .content-group .btn-float{
            padding:10px;
        }
        .content-group .btn-float i{
            font-size: 16px;
            font-weight: 300;
        }
        .content-group .btn-float:hover, .unlike, .unlike i{
            color:white !important;
            background: #F4511E !important;
        }
        .comment-list{
            max-height: 226px;
            overflow-y: auto;
            padding-left: 13px;
        }
        .view-post{
            margin-top:20px;
            padding:0;
        }
        .view-post:hover{
            cursor:pointer;
        }
        .post-avatar{
            width:30px;
            max-height: 30px !important;
            vertical-align: middle !important;
        }
        .list-feed-comments{
            background: white !important;
        }
        fieldset{
            min-height: 300px;
        }
        .preview.shown{
            width: 200px;
            height: 200px;
        }

        .preview{
            text-align: center;
            display: block;
            margin: 0 auto;
        }
        .preview img{
            margin:0 auto;
            text-align:center;
            width:100%;
        }
        .file-styled{
            width:100%;
        }
        #reset{
            display: none;
        }
        table.trending tr td{
            padding-left: 0;
            padding-right:0;
        }
    </style>
@endsection

@section('content')
<!-- Tabs widget -->
    <div class="col-md-4">
        <form method="GET" action="{{ route('search') }}">
            @csrf
            <table style="width:100%; height:100%; margin-bottom: 20px;">
                <tr>
                    <td>
                        <div class="input-group">
                            <input type="text" name="search" class="form-control" style="font-size:18px; height: 53px; " placeholder="Search...">
                            <span class="input-group-addon bg-primary" style="padding: 18px 20px;"><i class="fa fa-search" style="font-size: 16px;"></i></span>
                        </div>
                    </td>
                </tr>
            </table>
        </form>
    </div>
    <div class="col-md-8">
        <div class="panel panel-flat col-md-12" style="padding:0;">
                            <div class="table-responsive">
                                <table class="table text-nowrap">
                                    <tbody>   
                                        @foreach($mentions as $mention)
                                        <tr>
                                            <td style="width:20%">
                                                <div class="media-left media-middle">
                                                    <a href="#" class="btn bg-teal-400 btn-rounded btn-icon btn-xs">
                                                        <img src="{{ $mention->profile->user->avatar() }}" class="img-circle img-xs">
                                                    </a>
                                                </div>

                                                <div class="media-body">
                                                    <a href="#" class="display-inline-block text-default text-semibold letter-icon-title">{{ "@" . $mention->profile->handle }}</a>
                                                    <div class="text-muted text-size-small"> {{ convertTimeStamp($mention->created_at) }}</div>
                                                </div>
                                            </td>
                                            <td class="text-left" style="width: 70%;">
                                                <a href="#" class="text-default display-inline-block">
                                                    <span class="text-semibold">{!! mentionText($mention->from_id, $mention->type, $mention->post_id, false, $mention->id) !!}</span>
                                                </a>
                                            </td>
                                            <td class="text-center">
                                                <ul class="icons-list">
                                                    <li class="dropdown">
                                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="icon-redo2"></i></a>
                                                        <ul class="dropdown-menu dropdown-menu-right">
                                                            <li><a href="#"><i class="icon-undo"></i> Quick reply</a></li>
                                                            <li><a href="#"><i class="icon-history"></i> Full history</a></li>
                                                            <li class="divider"></li>
                                                            <li><a href="#"><i class="icon-checkmark3 text-success"></i> Resolve issue</a></li>
                                                            <li><a href="#"><i class="icon-cross2 text-danger"></i> Close issue</a></li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                    </div>
    </div>
    <div class="col-md-4">

        <div class="panel panel-flat col-md-12">
            <div class="panel-heading">
                <h6 class="panel-title trends"><i class="icon-statistics"></i> Trending</h6>
            </div>
            <div class="panel-body">
                <div class="trending">
                    <table class="table trending">
                        <tbody>
                            <tr>
                                <td><a href="#">#Something In Politics</a></td>
                                <td class="text-right">27.5K Posts</td>
                            </tr>
                            <tr>
                                <td><a href="#">¿Why do people support?</td>
                                <td class="text-right">27.5K Posts</td>
                            </tr>
                            <tr>
                                <td><a href="#">@JustusEwing</td>
                                <td class="text-right">27.5K Posts</td>
                            </tr>
                            <tr>
                                <td><a href="#">#Volcano</td>
                                <td class="text-right">27.5K Posts</td>
                            </tr>
                            <tr>
                                <td><a href="#">¿What do I add to powdered water?</td>
                                <td class="text-right">27.5K Posts</td>
                            </tr>
                            <tr>
                                <td><a href="#">@tiwebsoft</td>
                                <td class="text-right">27.5K Posts</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="panel panel-flat col-md-12" style="padding: 10px;">
            <div class="panel-body">
                <img src="/assets/images/square_ad.jpg" style="width:100%;">
            </div>
        </div>
    </div>
    
 <!-- /tabs widget -->
@endsection

@section('js')
    <script type="text/javascript" src="/assets/js/plugins/media/validate.js"></script>
    <script type="text/javascript">
        $("body").on("keyup", ".comment", function(event){
            if (event.keyCode === 13) {
                var today = moment().format("MMM. D, Y"); 
                var comment = $(this).val();
                var post = $(this).attr("data-post");
                if(comment != ""){
                    $.post( "{{ route('comment') }}", { post: post, comment: comment, _token: "{{ CSRF_TOKEN() }}"}, function(data, status){

                    });
                    $(this).parent().parent().parent().parent().parent().parent().find(".list-feed-comments").prepend('<li class="border-primary-300"><a href="{{ route("user", ["id" => Auth::user()->id]) }}"><img src="{{ Auth::user()->avatar() }}" class="comment-avatar img-circle"></a><a href="{{ route("user", ["id" => Auth::user()->id]) }}">{{"@" . Auth::user()->profile->handle }}</a> ' + comment + '<br>' + today + '</li>')
                    $(this).val("");
                }
            }
        });
        $("body").on("click", ".like", function(event){
            var post = $(this).attr("data-post");
            $.ajax({
                type: "GET",
                url: "{{ route('like', ['post' => null]) }}/" + post,
                async: false
            });
            $(this).removeClass("like");
            $(this).addClass("unlike");
        });
        $("body").on("click", ".unlike", function(event){
            var post = $(this).attr("data-post");
            $.ajax({
                type: "GET",
                url: "{{ route('unlike', ['post' => null]) }}/" + post,
                async: false
            });
            $(this).removeClass("unlike");
            $(this).addClass("like");
        });
        $("body").on("click", ".view-post", function(){
            window.location.href = $(this).attr("data-url");
        });
    </script>
@endsection
