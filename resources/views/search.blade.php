@extends('layouts.application')

@section('css')
    <style>

        .nav-tabs-highlight li a, .nav-tabs-highlight li a i{
            
        }
        .nav-tabs.nav-justified > li > a, .nav-tabs.nav-justified > li {
            border-bottom: 0;
            border-right: 0;
        }
        .tab-content-bordered .tab-content:not([class*=bg-]){
            background: none;
            border:0;
        }
        .tab-content, .tab-pane, .tab-content-bordered .tab-content > .has-padding{
            padding:0;
        }
        .search-tab{
            width: 650px !important;
            max-width: inherit !important;
            vertical-align: middle;
            background: white;
            padding:10px
            border-bottom:1px solid silver;
            height:51px;
        }
        .nav-tabs.nav-justified > li.search-tab{
            float:right;
            border-bottom: 1px solid #ddd;
            border-left: 1px solid #ddd;
            border-bottom: 1px solid #ddd;
            border-top: 1px solid #ddd;
        }
        .search-tab .form-control{
            border: none;
            height:51px;
        }
        .search-tab .input-group-addon{
                padding: 16px 17px;
                font-size: 18px;
        }
        .nav-tabs > li.active > a, .nav-tabs > li.active > a:hover, .nav-tabs > li.active > a:focus{
            color: #2196F3;
        }
        .view-post:hover{
            cursor:pointer;
        }
    </style>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-flat">
                <div class="panel-heading">
                    <h5 class="panel-title">Website search results<a class="heading-elements-toggle"><i class="icon-more"></i></a></h5>
                    <div class="heading-elements">
                        <ul class="icons-list">
                            <li><a data-action="collapse"></a></li>
                            <li><a data-action="close"></a></li>
                        </ul>
                    </div>
                </div>
                <div class="panel-body">
                    <form  class="main-search" method="GET" action="{{ route('search') }}">
                    @csrf
                        <div class="input-group content-group">
                            <div class="has-feedback has-feedback-left">
                                <input type="text" name="search" class="form-control input-xlg" value="{{ $query }}" required="">
                                <div class="form-control-feedback">
                                    <i class="icon-search4 text-muted text-size-base"></i>
                                </div>
                            </div>

                            <div class="input-group-btn">
                                <button type="submit" class="btn btn-primary btn-xlg">Search</button>
                            </div>
                        </div>

                        <div class="row search-option-buttons">
                            <div class="col-sm-6">
                                <ul class="list-inline list-inline-condensed no-margin-bottom">
                                    <li class="dropdown">
                                        <a href="#" class="btn btn-link dropdown-toggle" data-toggle="dropdown">
                                            <i class="icon-stack2 position-left"></i> All categories <span class="caret"></span>
                                        </a>

                                        <ul class="dropdown-menu">
                                            <li><a href="#"><i class="icon-question7"></i> Getting started</a></li>
                                            <li><a href="#"><i class="icon-accessibility"></i> Registration</a></li>
                                            <li><a href="#"><i class="icon-reading"></i> General info</a></li>
                                            <li><a href="#"><i class="icon-gear"></i> Your settings</a></li>
                                            <li><a href="#"><i class="icon-graduation"></i> Copyrights</a></li>
                                            <li class="divider"></li>
                                            <li><a href="#"><i class="icon-mail-read"></i> Contacting authors</a></li>
                                        </ul>
                                    </li>
                                    <li><a href="#" class="btn btn-link"><i class="icon-reload-alt position-left"></i> Refine your search</a></li>
                                </ul>
                            </div>

                            <div class="col-sm-6 text-right">
                                <ul class="list-inline no-margin-bottom">
                                    <li><a href="#" class="btn btn-link"><i class="icon-make-group position-left"></i> Browse website</a></li>
                                    <li><a href="#" class="btn btn-link"><i class="icon-menu7 position-left"></i> Advanced search</a></li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="tab-content-bordered content-group">
                <ul class="nav nav-tabs nav-tabs-highlight nav-lg nav-justified">
                    <li class="active"><a href="#tab-flash" data-toggle="tab"><i class="fa fa-palette"></i> Posts {{ $posts->count() }}</a></li>
                    <li><a href="#tab-blogs" data-toggle="tab"><i class="fa fa-palette"></i> Blogs {{ $blogs->count() }}</a></li>
                    <li><a href="#tab-mentions" data-toggle="tab"><i class="fa fa-users"></i> Users {{ $profiles->count() }}</a></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane has-padding active" id="tab-flash">
                        <div class="row posts-timeline">
                            <div class="col-md-12">
                                @foreach($posts as $post)
                                <div class="panel panel-flat col-md-12">
                                    <div class="panel-heading">
                    <!-- Blog layout #4 with image -->
                                        <h6 class="panel-title" style="position:absolute; left: 20px; z-index: 4;"><a href="{{ route('user', ['id' => $post->user]) }}"><img src="{{ $post->user->avatar() }}" class="post-avatar img-circle" alt=""> {{ "@" . $post->user->profile->handle }}</a> </h6>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row" style="@if(!isset($post->featured))background-image: url('/assets/images/text-post.jpg'); background-size: cover; background-position: left bottom;@endif">
                                            <div class="col-md-12 view-post" data-url="{{ route('view-post', ['post' => $post->id]) }}">
                                                @if(isset($post->featured))
                                                <div class="thumb content-group" style="margin-bottom: 0 !important;">
                                                    <h5 class="text-left" style="padding-left:20px;">{{ $post->content }}</h5>
                                                    <img src="/uploads/media/{{ $post->featured() }}" style="border-radius: 0;">
                                                </div>
                                                @else
                                                <div class="thumb content-group">
                                                    <h2 style="line-height: 200px;"><span class="quote"><i class="fa fa-quote-left"></i></span> {{ $post->content }} <span class="quote"><i class="fa fa-quote-right"></i></span></h2>
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                    <!-- /blog layout #4 with image -->
                                </div>
                                @endforeach
                            @if($posts->count() == 0)
                                <div class="panel panel-flat col-md-12">
                                    <div class="panel-heading">
                                        <h2>No Results</h2>
                                    </div>
                                </div>
                            @endif
                            </div>
                        </div>
                    </div>
                <div class="tab-pane has-padding" id="tab-blogs">
                    <div class="row posts-timeline">
                        <div class="col-md-12">
                            @foreach($blogs as $blog)
        <!-- Blog layout #2 with image -->
        <div class="col-md-12 posts subject-{{ $blog->subject_id or '' }} shade-{{ $blog->shade_id or '' }}" data-subject="subject-{{ $blog->subject_id or '0' }}" data-shade="shade-{{ $blog->shade_id or '0' }}">
            <div class="panel panel-flat">
                <div class="panel-body">
                    <div class="thumb content-group">
                        @if(is_object($blog->media))
                            <img src="/uploads/media/{{ $blog->media->path }}" alt="" class="img-responsive">
                            
                        @endif
                        <div class="caption-overflow">
                            <span>
                                <a href="/podium/view/{{ $blog->id }}" class="btn btn-flat border-white text-white btn-rounded btn-icon"><i class="icon-arrow-right8"></i></a>
                            </span>
                        </div>
                    </div>
                    <h5 class="text-semibold content-group-sm">
                        <a href="#" class="text-default">{!! $blog->title !!}</a>
                    </h5>
                </div>

                <div class="panel-footer panel-footer-condensed">
                    <div class="heading-elements not-collapsible">
                        <ul class="list-inline list-inline-separate heading-text text-muted">
                            <li>By <a href="{{ route('user', ['user' => $blog->user_id]) }}" class="text-muted">{{ "@" . $blog->user->profile->handle }}</a></li>
                            <li>{{ convertTimeStamp($blog->created_at) }}</li>
                            <li><a href="#" class="text-muted">12 comments</a></li>
                            <li><a href="#" class="text-muted"><i class="icon-heart6 text-size-base text-pink position-left"></i> 281</a></li>
                        </ul>

                        <a href="/podium/view/{{ $blog->id }}" class="heading-text pull-right">Read more <i class="icon-arrow-right14 position-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
        <!-- /blog layout #2 with image -->
    @endforeach
                            @if($blogs->count() == 0)
                                <div class="panel panel-flat col-md-12">
                                    <div class="panel-heading">
                                        <h2>No Results</h2>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="tab-pane has-padding" id="tab-mentions">
                    <div class="row">
                        <div class="col-md-12">
                            @if($profiles->count() == 0)
                                <div class="panel panel-flat col-md-12">
                                    <div class="panel-heading">
                                        <h2>No Results</h2>
                                    </div>
                                </div>
                                @else
                                <div class="panel panel-flat" style="padding:0;">
                                    <div class="panel-body">
                                        <div class="search-results-list">
                                            @foreach($profiles as $profile)
                                                <div class="col-lg-3 col-sm-6">
                                                    <div class="thumbnail">
                                                        <div class="thumb thumb-rounded">
                                                            <img src="{{ $profile->user->avatar() }}" alt="">
                                                            <div class="caption-overflow">
                                                                <span>
                                                                    <a href="{{ route('user', ['user' => $profile->user_id]) }}" class="btn border-white text-white btn-flat btn-icon btn-rounded ml-5"><i class="icon-link2"></i></a>
                                                                </span>
                                                            </div>
                                                        </div>
                                                        <div class="caption text-center">
                                                            <h6 class="text-semibold no-margin"><a href="{{ route('user', ['user' => $profile->user_id]) }}" class="">{{ "@" . $profile->handle }}</a></h6>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach

                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                </div>
                </div>
            </div>
        </div>
            <div class="col-md-4">
        <div class="panel panel-flat col-md-12" style="padding:20px;">
            <div class="panel-body">
                <img src="/assets/images/square_ad.jpg" style="width:100%;">
            </div>
        </div>
    </div>
    </div>

@endsection

@section('js')
    <script type="text/javascript">
        $("body").on("click", ".view-post", function(){
            window.location.href = $(this).attr("data-url");
        });
    </script>
@endsection
