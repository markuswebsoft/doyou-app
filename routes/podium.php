<?php

Route::prefix('podium')->group(function () {
	Route::get('/', 'PodiumController@index')->name('user-podium');

	Route::get('/s/{s}', 'PodiumController@filter');

	Route::get('/new', 'PodiumController@new')->name('new-podium-post');
	
	Route::get('/view/{blog}', 'PodiumController@view');

	Route::post('/new', 'PodiumController@publish')->name('publish-podium');

	Route::post('/confirmurl', 'PodiumController@confirmPodURL')->name('confirmPodURL');

	Route::get('/edit/{blog}', 'PodiumController@edit')->name('edit-podium');

	Route::post('/edit', 'PodiumController@update')->name('update-podium');
});

Route::prefix('pod')->group(function () {
	Route::get('/{url}', 'PodController@view');
});