<?php

Route::prefix('post')->group(function () {
	Route::get('like/{post}', 'PostsController@like')->name('like');
	Route::get('unlike/{post}', 'PostsController@unlike')->name('unlike');
	Route::get('edit/{post}', 'PostsController@edit')->name('edit-post');
	Route::get('view/{post}', 'PostsController@view')->name('view-post');

	Route::post('publish', 'PostsController@publish')->name('publish');
	Route::post('comment', 'PostsController@comment')->name('comment');
	Route::post('comment/reply', 'PostsController@commentReply')->name('commentReply');
	Route::post('like', 'PostsController@update')->name('update-post');

	Route::delete('/', 'PostsController@deletePost')->name('deletePost');
});